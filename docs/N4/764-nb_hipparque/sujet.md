---
author: Franck Chambon
title: Nombres d'Hipparque
tags:
  - 4-maths
  - 6-récursivité
  - 7-mémo
---
# Subdivisions de polygone

Les nombres d'Hipparque sont nommés d'après le mathématicien et astronome grec, qui, selon Plutarque, connaissait certainement ces nombres.

!!! info "Culture"
    - Hipparque a été actif en Grèce, au moins entre 147 et 127 av. J.-C.
    - Plutarque est un philosophe majeur de la Rome antique, qui a vécu de 46 à 125, soit deux siècles après Hipparque.

>![](./Pentagon_subdivisions.svg)
>
>Les $s_4=11$ subdivisions d'un pentagone

On admettra que :

- Le $n$-ième nombre $s_n$ de la suite est le nombre de subdivisions d'un polygone à $n + 1$ côtés en polygones plus petits par l'adjonction de diagonales au polygone de départ.
- $s_2=1$, et $s_3=3$.
- Une formule pour calculer $s_n$ avec $n>3$ est :

$$s_n = \frac{\left((6n-9)s_{n-1} - (n-3)s_{n-2}\right)}n$$

Écrire une fonction telle que `hipparque(n)` renvoie le nombre $s_n$ pour $1 < n <100$.

!!! example "Exemples"

    ```pycon
    >>> hipparque(3)
    3
    >>> hipparque(4)
    11
    >>> hipparque(5)
    45
    ```

{{ IDE('exo', MAX=1000) }}
