## Commentaires

### Solution

{{ IDE('exo_corr') }}

### Complément

Cette méthode se généralise pour déterminer la valuation $p$-adique d'un entier $n$.

Si $n$ est un entier naturel, et $p$ un nombre premier, la formule de Legendre donne l'exposant de $p$ dans la décomposition en facteur premier de $n!$.

$$v_p(n!) = \sum_{k=1}^{\infty} \lfloor \frac{n}{p^k} \rfloor = \lfloor \frac{n}{p} \rfloor + \lfloor \frac{n}{p^2} \rfloor + \lfloor \frac{n}{p^3} \rfloor + \cdots$$

- La somme s'arrête ; tous les termes sont nuls à partir d'un certain rang.
- $\lfloor \frac{a}{b} \rfloor$ est la partie entière de $a$ divisé par $b$. Avec Python, on peut simplement faire `a // b`.

Cela donne une fonction Python

```python
def valuation(n, p):
    """Renvoie la valuation p-adique de n!
    - n est un entier naturel
    - p est un nombre premier
    """
    resultat = 0
    puiss_p = p
    while puiss_p <= n:
        resultat += n // puiss_p
        puiss_p *= p
    return resultat
```

On peut se servir de cette fonction pour obtenir la décomposition complète en facteurs premiers de $n!$, mais aussi de coefficients binomiaux qui se calculent avec des factorielles.

Il faudra juste avoir une liste de nombres premiers... C'est toujours très utile.

