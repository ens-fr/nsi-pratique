---
author: Franck Chambon
title: Fibonacci (2)
tags:
  - 4-maths
  - 6-récursivité
---

# Suite de Fibonacci (2)

Les premiers termes de la suite de Fibonacci sont :

$$0, 1, 1, 2, 3, 5, 8, 13, 21, 34, \cdots$$

- Les deux premiers termes sont $0$ et $1$.
- À partir du troisième, un terme est la somme des deux précédents.

Il existe une formule récursive efficace pour calculer **deux nombres de Fibonacci consécutifs**.

On pourra poser $F_{-1} = 1$ de sorte que $F_{-1} + F_0 = F_1$.

Pour $m>0$, on admet que :

$$\begin{cases}
F_{2k-1} = F_{k}^{2} + F_{k-1}^{2}\\
F_{2k} = F_{k}^{2} + 2×F_{k}×F_{k-1}\\
F_{2k+1} = F_{2k} + F_{2k-1}
\end{cases}$$

Écrire une fonction récursive `fib_2` telle que `fib_2(n)` renvoie le couple $(F_{n-1}, F_n)$

!!! abstract "Méthode appliquée à `n = 9`"
    - L'appel `fib_2(9)` souhaite renvoyer $(F_8, F_9)$
    - $9$ est impair, on va demander $(F_7, F_8)$, on pourra en déduire $F_9$
    - On appelle `fib_2(8)`
    - $8$ est pair, on va demander $(F_3, F_4)$
    - On appelle `fib_2(4)`
    - $4$ est pair, on va demander $(F_1, F_2)$
    - On appelle `fib_2(2)`
    - $2$ est pair, on va demander $(F_0, F_1)$
    - On appelle `fib_2(1)`
    - $1$ est un cas de base, `fib_2(1)` renvoie `(0, 1)`
    - On déduit
        - $F_1 = F_1^2 + F_0^2 = 1$
        - $F_2 = F_1^2 + 2×F_1×F_0 = 1$
        - l'appel `fib_2(2)` renvoie `(1, 1)`
    - On déduit
        - $F_3 = F_2^2 + F_1^2 = 2$
        - $F_4 = F_2^2 + 2×F_2×F_1 = 3$
        - l'appel `fib_2(4)` renvoie `(2, 3)`
    - On déduit
        - $F_7 = F_4^2 + F_3^2 = 13$
        - $F_8 = F_4^2 + 2×F_4×F_3 = 21$
        - l'appel `fib_2(8)` renvoie `(13, 21)`
    - On déduit
        - $F_9 = F_8 + F_7 = 34$
        - l'appel `fib_2(9)` renvoie `(21, 34)`

??? tip "Indice : Algorithme"
    - Si `n` est un cas de base, on renvoie la réponse directement.
    - Sinon,
        - Si `n` est pair, alors
            - on pose `k = n // 2`
            - on fait un appel récursif avec `k`
            - on déduit la réponse pour `n`
        - Sinon, `n` est impair, et alors
            - on pose `k = n - 1`
            - on fait un appel récursif avec `k`, qui est pair, lui
            - on déduit la réponse pour `n`

!!! example "Exemples"

    ```pycon
    >>> fib_2(0)
    (1, 0)
    >>> fib_2(1)
    (0, 1)
    >>> fib_2(2)
    (1, 1)
    >>> fib_2(3)
    (1, 2)
    >>> fib_2(9)
    (21, 34)
    >>> fib_2(4)
    (2, 3)
    ```

{{ IDE('exo', MAX=1000) }}

