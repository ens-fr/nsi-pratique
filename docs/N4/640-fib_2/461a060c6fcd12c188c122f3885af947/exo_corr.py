def fib_2(n):
    """Renvoie (F_{n - 1}, F_{n})
        deux nombres de Fibonacci consécutifs
    on utilise les variables f_nm1 et f_n
    """
    # cas de base
    if n == 0:
        f_nm1 = 1
        f_n = 0
    elif n == 1:
        f_nm1 = 0
        f_n = 1
    
    # cas général
    elif n % 2 == 0:
        # n est pair
        k = n // 2
        f_km1, f_k = fib_2(k)
        f_nm1 = f_k * f_k  +  f_km1 * f_km1
        f_n = f_k * (f_k  +  2 * f_km1)
    else:
        # n est impair
        k = n - 1
        f_km1, f_k = fib_2(k)
        f_nm1 = f_k
        f_n = f_km1 + f_k

    return f_nm1, f_n
