def fib_2(n, m):
    """Renvoie (F_{n-1}, F_{n}) chacun modulo m
        deux nombres de Fibonacci consécutifs
    on utilise les variables f_nm1 et f_n
    """
    # cas de base
    if n == 0:
        f_nm1 = 1
        f_n = 0
    elif n == 1:
        f_nm1 = 0
        f_n = 1
    
    # cas général
    elif n % 2 == 0:
        # n est pair
        k = n // 2
        f_km1, f_k = fib_2(k, m)
        f_nm1 = f_k * f_k  +  f_km1 * f_km1
        f_n = f_k * f_k  +  2 * f_km1
    else:
        # n est impair
        k = n - 1
        f_km1, f_k = fib_2(k, m)
        f_nm1 = f_k
        f_n = (f_km1 + f_k) % m

    return f_nm1 % m, f_n % m


f_nm1, f_n = fib_2(10**16, 10**9)
print(f_n)
