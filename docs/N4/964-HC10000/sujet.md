---
author: Franck Chambon
title: Hofstadter-Conway 10000$
tags:
  - 4-maths
  - 6-récursivité
  - 9-prog.dynamique
---

# Suite de Hofstadter-Conway à 10000$

Cette suite $(a_n)_{n\in\mathbb N^*}$ de Hofstadter-Conway est définie par :

$$a_1 = a_2 = 1$$

$$a_n = a_{a_{n-1}} + a_{n-a_{n-1}}\quad \text{pour }n \geqslant 3$$

On définit alors

$$S_n = \sum_{i=1}^{n}a_n\quad \text{pour }n \geqslant 1$$

Objectif
    : Construire un tableau de valeurs pour $S$ de taille 10000. $S_0$ n'étant pas défini, on le codera par `#!py None`.

!!! example "Exemples"
    ```pycon
    >>> S[1]
    1
    >>> S[2]
    2
    >>> S[3]
    4
    >>> S[4]
    6
    >>> len(S) >= 10000
    True
    ```

{{ IDE('exo', MAX=1000) }}

!!! info "Histoire des $10\,000\,\$$"
    Le 15 juillet 1988, pendant un colloque au laboratoire Bell, John Conway a affirmé qu'il était capable de prouver que $\lim_{n\to\infty} \frac{a(n)}n \to \frac12$, mais que la preuve était extrêmement difficile.
    
    Il a alors proposer d'offrir $100\,\$$ à quiconque pourrait trouver un $n_0$ tel que pour tout $n\geqslant n_0$, on a $|\frac{a_n}n - \frac12| < 0.05$, et qu'il offrirait $10\,000\,\$$ pour le plus petit $n_0$ satisfaisant. (Il aurait voulu dire $1000\,\$$). Le prix a été gagné par Colin Mallows, qui a accepté de ne pas encaisser le chèque.
