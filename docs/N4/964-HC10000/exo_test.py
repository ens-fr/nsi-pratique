# tests

assert S[1] == 1
assert S[2] == 2
assert S[3] == 4
assert S[4] == 6
assert len(S) >= 10000

# autres tests
debut = [None, 1, 2, 4, 6, 9, 13, 17, 21, 26, 32, 39, 46, 54, 62, 70, 78, 87, 97, 108]
fin = [26303585, 26308945, 26314305, 26319665, 26325025, 26330386, 26335748, 26341111, 26346475, 26351840, 26357205, 26362571, 26367938, 26373306, 26378675, 26384044, 26389414, 26394785, 26400157, 26405529]
attendu = debut
assert S[:20] == attendu
attendu = fin
assert S[(10000 - 20):10000] == attendu

