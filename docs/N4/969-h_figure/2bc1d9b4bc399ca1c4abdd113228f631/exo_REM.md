## Commentaires

{{ IDE('exo_corr') }}

Au début,

```python
R = [None, 1]
S = [None, 2]
```

- On déduit `R[2] = 3`.
- Les suites sont **complémentaires**, donc l'entier `4` est dans `R` ou `S`.
    - Il ne peut pas être dans `R` ; le prochain sera `3 + ???` (pas 1, ni 2), donc au moins 6.
    - Ainsi `S` se poursuit avec `4` et `5` ...

```python
R = [None, 1, 3]
S = [None, 2, 4, 5]
```

- On déduit `R[3] = 7` et que `6` est dans `S`.
- De là, on peut initier la boucle en Python.

```python
R = [None, 1, 3]  # 7 à venir
S = [None, 2, 4, 5, 6]
```



!!! info "GEB"
    [Douglas Hofstadter](https://fr.wikipedia.org/wiki/Douglas_Hofstadter) est l'auteur du livre [Gödel, Escher, Bach : Les Brins d'une Guirlande Éternelle](https://fr.wikipedia.org/wiki/G%C3%B6del,_Escher,_Bach_:_Les_Brins_d%27une_Guirlande_%C3%89ternelle) qui a obtenu le prix Pulitzer.

    On y trouve en particulier certaines suites étonnantes.

    ![Par Max Braun — https://www.flickr.com/photos/maxbraun/3196699274, CC BY-SA 2.0, https://commons.wikimedia.org/w/index.php?curid=75349761](../Godel_escher_bach_abmigram.png)

    > Ambigramme G E B, initiales de Gödel, Escher et Bach présent sur la couverture du livre dans l'édition de 1979. Par Max Braun.
