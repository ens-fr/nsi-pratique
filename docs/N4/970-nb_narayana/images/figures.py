def gen(n, k):
    if n == k == 1:
        yield []
    elif k == n - 1:
        yield [[] for _ in range(k)]
    elif (k > n - 1) or (k <= 0) or (n <= 0):
        pass
    else:
        for dessous in gen(n-1, k):
            yield [dessous]
        for n1 in range(1, n-1):
            for k1 in range(1, k + 1):
                for gauche in gen(n1, k1):
                    for droite in gen(n - n1, k - k1):
                        yield [gauche, *droite]


"""
Copyright Franck CHAMBON
CC 4.0 BY SA
Pas d'utilisation commerciale
"""

import drawSvg as draw

def ajoute_grille(dessin, n, m, pas):
    def ajoute_ligne(x1, y1, x2, y2):
        dessin.append(draw.Lines(x1, y1, x2, y2,
            close=False, stroke_width=2, stroke='grey'))

        for x, y in [(x1, y1), (x2, y2)]:
            dessin.append(draw.Circle(x, y, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))

    for i in range(n + 1):
        ajoute_ligne(0*pas, i*pas, m*pas, i*pas)
    for j in range(m + 1):
        ajoute_ligne(j*pas, 0*pas, j*pas, n*pas)

def ajoute_chemin_lukasiewicz(dessin, chemin, pas):
    x1, y1 = 0, 0
    dessin.append(draw.Circle(x1, y1, 3,
            fill='red', stroke_width=2, stroke='red'))
    dx = 1
    for dy in chemin:
        x2, y2 = x1 + pas*dx, y1 + pas*dy
        dessin.append(draw.Lines(
            x1, y1, x2, y2,
            close=False, fill='none', stroke_width=4, stroke='red'))
        dessin.append(draw.Circle(x2, y2, 3,
                fill='red', stroke_width=2, stroke='red'))
        x1, y1 = x2, y2


def arbre_vers_chemin(arbre):
    "correspondance de Łukasiewicz"
    def etape(arbre):
        "Fonction récursive interne"
        resultat = [len(arbre)]
        for sous_arbre in arbre:
            resultat.extend(etape(sous_arbre))
        return resultat

    chemin = [y - 1 for y in etape(arbre)]
    chemin.pop()
    return chemin


def chemin_vers_arbre(chemin):
    "correspondance de Łukasiewicz"
    def etape(temp, i):
        "Fonction récursive interne"
        if temp[i] == 0:
            return [], 1
        else:
            resultat = []
            taille_totale = 1
            for _ in range(temp[i]):
                sous_arbre, taille = etape(temp, i + taille_totale)
                taille_totale += taille
                resultat.append(sous_arbre)
        return resultat, taille_totale

    n = 1 + len(chemin)
    temp = [y + 1 for y in chemin] + [0]
    arbre, taille = etape(temp, 0)
    return arbre


bulle, dx, dy = 4, 16, 32
def largeur(arbre):
    if arbre == []:
        return bulle
    else:
        return (
            sum(largeur(sous_arbre) for sous_arbre in arbre)
            + dx * (len(arbre) - 1)
        )

def hauteur(arbre):
    if arbre == []:
        # c'est une feuille, pas un arbre vide !!!
        return 0
    else:
        return dy + max(hauteur(sous_arbre) for sous_arbre in arbre)

def ajoute_arbre(dessin, arbre, x1, y1):
    trait_couleur = 'grey'
    noeud_couleur = 'red'

    x2 = x1 - largeur(arbre) // 2
    y2 = y1 - dy
    for sous_arbre in arbre:
        x2 += largeur(sous_arbre) // 2
        dessin.append(draw.Lines(
            x1, y1, x2, y2,
            close=False, fill='none', stroke_width=4, stroke=trait_couleur))
        dessin.append(draw.Circle(x2, y2, bulle,
                fill=noeud_couleur, stroke_width=2, stroke=trait_couleur))
        ajoute_arbre(dessin, sous_arbre, x2, y2)
        x2 += largeur(sous_arbre) // 2 + dx
    dessin.append(draw.Circle(x1, y1, bulle,
            fill=noeud_couleur, stroke_width=2, stroke=trait_couleur))


for (n, k) in [(4, 2), (5, 3)]:
    n += 1
    pas = 16
    marge = 8

    for i, arbre in enumerate(gen(n, k)):
        #print(arbre)

        l = largeur(arbre)
        h = hauteur(arbre)

        d = draw.Drawing(
            l + 2*marge,
            h + 2*marge,
            origin=(-l // 2 - marge, - marge),
            displayInline=False
        )

        ajoute_arbre(d, arbre, 0, h)
        d.saveSvg(f'arbre_{n}_{k}_{i}.svg')
        print(f"![](images/arbre_{n}_{k}_{i}.svg)")


for n in range(12):
    for k in range(1, n+1):
        print(sum(1 for _ in gen(n, k)), end=", ")
    print()
