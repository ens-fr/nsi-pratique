## Commentaires

{{ IDE('exo_corr') }}

On peut remplacer les lignes 6 à 10 par des listes en compréhension.

```python
new_croissant = [sum(serpent_decroissant[j] for j in range(0, i)) for i in range(10)]
new_decroissant = [sum(serpent_croissant[j] for j in range(i+1, 10)) for i in range(10)]
```

À la suite de quoi, il est possible de se passer des tableaux `new_*` de manières explicites. Ils seront toutefois toujours créés par Python !!! Le code de la boucle devient alors

```python
for _ in range(2, 100):
    serpent_croissant, serpent_decroissant = (
        [sum(serpent_decroissant[j] for j in range(0, i)) for i in range(10)],
        [sum(serpent_croissant[j] for j in range(i+1, 10)) for i in range(10)],
    )
    serpent.append(sum(serpent_croissant) + sum(serpent_decroissant))
```

On a ici un style plutôt fonctionnel très dense.

## Pour aller plus loin

Ici, on calcule les termes de `serpent` de proche en proche.

En utilisant du calcul matriciel, ou mieux polynomial, on pourra calculer des termes éloignés de `serpent`. Il faudra maitriser l'exponentiation rapide et le calcul modulaire.
