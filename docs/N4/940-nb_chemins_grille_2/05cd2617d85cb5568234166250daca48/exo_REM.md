## Commentaires

### Solution

{{ IDE('exo_corr') }}

Pour calculer un produit, on initialise le produit à 1, et non à 0.

- Une somme vide est égale à 0.
- Un produit vide est égal à 1.

Pour la boucle, on peut écrire `resultat *= facteur` à la place de `resultat = resultat * facteur`. De la même manière qu'on peut écrire `resultat += terme` à la place de `resultat = resultat + terme`

Il existe aussi les raccourcis

- `-=`, pour la soustraction
- `//=`, pour la division entière
- `%=`, pour le modulo
- `/=`, pour la division continuée
- `**=`, pour la puissance
- `<<=` pour le décalage de bits à gauche
- `>>=` pour le décalage de bits à droite


### Formule à justifier ; maths

Justifions la formule : `nb_chemins(n, m)` est égal à $\binom{n}{n+m}$.

$$\binom{n}{n+m} = \dfrac{(n+m)!}{n!m!}$$

> On rappelle que pour compter le nombre de façons de choisir $k$ éléments parmi $n$, on utilise le coefficient binomial $\binom{k}{n}`.


Un chemin allant de $(0, 0)$ jusqu'à $(n, m)$ peut être décrit par une suite de `E` et de `N` ; `E` pour aller à l'Est, `N` pour aller au Nord. Il doit y avoir $n$ fois la présence de `E`, et $m$ fois la présence de `N`.

Il y a autant de chemins allant de $(0, 0)$ jusqu'à $(n, m)$ que de mots de $n+m$ lettres composés de $n$ `E` et $m$ `N`. Pour compter ces mots, il suffit de compter le nombre de façons de choisir $n$ positions parmi les $n+m$ pour placer les `E`. On remarquera que $\binom{n}{n+m} = \binom{m}{n+m}$, et choisir les `N` impose les `E` tout autant. Ce qui justifie la formule donnée.
