# 🗒️ Présentation

Dans cette catégorie, on trouve des exercices plus mathématiques qui intéresseront les élèves qui veulent aller, ou qui sont déjà, en MP2I.

![](./Noether.jpg)

> [Amalie Emmy Noether](https://fr.wikipedia.org/wiki/Emmy_Noether) (23 mars 1882 – 14 avril 1935) est une mathématicienne allemande spécialiste d'algèbre abstraite et de physique théorique. Considérée par Albert Einstein comme « le génie mathématique créatif le plus considérable produit depuis que les femmes ont eu accès aux études supérieures », elle a révolutionné les théories des anneaux, des corps et des algèbres. En physique, le théorème de Noether explique le lien fondamental entre la symétrie et les lois de conservation et est considéré comme aussi important que la théorie de la relativité.
