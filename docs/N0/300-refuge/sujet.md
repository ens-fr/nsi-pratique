---
author: Charles Poulmaire
title: Refuge d'animaux
tags:
    - 3-dictionnaire
---

# Liste dictionnaire

Le refuge Wouaf_Miaou s'occupe de chats et de chiens pouvant être adoptés.
Compléter la fonction `selection_enclos` afin de renvoyer la liste de noms d'animaux d'un même enclos. L'ordre de rangement des noms dans la liste n'a pas d'importance.

!!! example "Exemples"

    ```pycon
    >>> Wouaf_Miaou = [
            {'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2},
            {'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
            {'nom':'Tom', 'espece':'chat', 'age':7, 'enclos':4},
            {'nom':'Belle', 'espece':'chien', 'age':6, 'enclos':3},
            {'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5},
    ]
    >>> sorted(selection_enclos(Wouaf_Miaou, 5))
    ['Mirza', 'Titine']
    >>> selection_enclos(Wouaf_Miaou, 2)
    ['Medor']
    >>> selection_enclos(Wouaf_Miaou, 7)
    []
    ```

!!! info "Remarque"

    L'ordre de parcours d'un dictionnaire dans Python se fait dans un ordre arbitraire. On utilise `sorted` dans les tests pour que les éléments de la réponse soient forcément dans l'ordre alphabétique et ainsi pouvoir les comparer avec les résultats attendus.
    
{{ IDE('exo') }}
