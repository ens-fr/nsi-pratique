def selection_enclos(refuge, numero_enclos):
    noms = []
    for animal in refuge:
        if animal['enclos'] == ...:
            noms.append(...)
    return noms

# Tests

Wouaf_Miaou = [
    {'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2},
    {'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
    {'nom':'Tom', 'espece':'chat', 'age':7, 'enclos':4},
    {'nom':'Belle', 'espece':'chien', 'age':6, 'enclos':3},
    {'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5},
]

assert sorted(selection_enclos(Wouaf_Miaou, 5)) == ['Mirza', 'Titine']
assert selection_enclos(Wouaf_Miaou, 2) == ['Medor']
assert selection_enclos(Wouaf_Miaou, 7) == []

