# Tests

Wouaf_Miaou = [
    {'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2},
    {'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
    {'nom':'Tom', 'espece':'chat', 'age':7, 'enclos':4},
    {'nom':'Belle', 'espece':'chien', 'age':6, 'enclos':3},
    {'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5},
]

assert sorted(selection_enclos(Wouaf_Miaou, 5)) == ['Mirza', 'Titine']
assert selection_enclos(Wouaf_Miaou, 2) == ['Medor']
assert selection_enclos(Wouaf_Miaou, 7) == []

## Tests supplémentaires

refuge = [
    {'nom': 'Rex', 'espece': 'chat', 'age': 5, 'enclos': 10},
    {'nom': 'Molosse', 'espece': 'chat', 'age': 1, 'enclos': 11},
    {'nom': 'Goloum', 'espece': 'chat', 'age': 7, 'enclos': 11},
    {'nom': 'Domino', 'espece': 'chien', 'age': 7, 'enclos': 10},
    {'nom': 'Fifille', 'espece': 'chien', 'age': 8, 'enclos': 10},
    {'nom': 'Muche', 'espece': 'chat', 'age': 4, 'enclos': 13},
]

assert sorted(selection_enclos(refuge, 10)) == ['Domino', 'Fifille', 'Rex']
assert selection_enclos(refuge, 14) == []
assert selection_enclos(refuge, 13) == ['Muche']
