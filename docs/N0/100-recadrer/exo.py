def recadre(mesures, val_min, val_max):
    for i in range(len(...)):
        if mesures[i] < ...:
            mesures[i] = ...
        elif ...:
            ...

# Tests
mesures_1 = [1, 2, 3, 4, 5, 6]
recadre(mesures_1, 3, 5)
assert mesures_1 == [3, 3, 3, 4, 5, 5]

mesures_2 = [7.1, -9.0, -3.1, 15.0, 987.7, -624.89]
recadre(mesures_2, -5.3, 15.0)
assert mesures_2 == [7.1, -5.3, -3.1, 15.0, 15.0, -5.3]

