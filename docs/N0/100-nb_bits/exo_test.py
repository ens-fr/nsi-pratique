# Tests
assert nb_bits(1) == 1
assert nb_bits(2) == 2
assert nb_bits(3) == 2
assert nb_bits(4) == 3

# Tests supplémentaires
for n in range(1, 1001):
    attendu = (n).bit_length()
    assert nb_bits(n) == attendu, f"Erreur avec {n=}"

# Test de l'utilisation de // et pas /
assert nb_bits(2**54 - 1) == 54, "Pensez à utiliser // au lieu de /"
