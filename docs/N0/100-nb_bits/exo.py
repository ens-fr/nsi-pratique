def nb_bits(n):
    ...


# Tests
assert nb_bits(1) == 1
assert nb_bits(2) == 2
assert nb_bits(3) == 2
assert nb_bits(4) == 3
