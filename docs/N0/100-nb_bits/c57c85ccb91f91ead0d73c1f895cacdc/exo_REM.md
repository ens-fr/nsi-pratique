## Commentaires

{{ IDE('exo_corr') }}

Il s'agit donc de compter combien de divisions euclidiennes sont nécessaires avant que le quotient ne soit nul.

Traduite en Python cette condition devient : `#!py while n != 0:`, on continue tant que $n$ n'est **pas nul**.

Cet algorithme fonctionne donc pour tous les entiers strictement positifs. La version ci-dessous permet de le rendre fonctionnel pour tous les entiers positifs : $0$ compris.

```python
def nb_bits(n):
    n = n // 2      # une division dès le départ et 0 // 2 vaut 0
    compteur = 1    # on a donc 1 bit minimum quoi qu'il arrive
    while n != 0:
        n = n // 2
        compteur = compteur + 1
    return compteur
```

Notons enfin qu'il est aussi possible de résoudre ce problème en utilisant une fonction récursive (qui s'appelle elle-même) :

```python
def nb_bits(n):
    if n < 2:
        return 1
    else:
        return 1 + nb_bits(n // 2)
```

Cette formulation traduit deux choses :

* il n'existe que deux nombres entiers positifs s'écrivant sur un seul bit : $0$ et $1$,
* l'écriture binaire de chaque **autre** nombre entier nécessite 1 bit de plus que celle de son quotient par $2$.
