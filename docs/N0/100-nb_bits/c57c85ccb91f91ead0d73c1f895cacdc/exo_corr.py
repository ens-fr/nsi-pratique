def nb_bits(n):
    compteur = 0
    while n != 0:
        n = n // 2
        compteur = compteur + 1
    return compteur


# Tests
assert nb_bits(1) == 1
assert nb_bits(2) == 2
assert nb_bits(3) == 2
assert nb_bits(4) == 3
