
# tests

assert perimetre(2, 3) == 10
assert perimetre(5, 2) == 14


# autres tests

for l in range(20):
    for L in range(20):
        attendu = (L + l) * 2
    assert perimetre(L, l) == attendu, f"Erreur avec {L=} et {l=}"
