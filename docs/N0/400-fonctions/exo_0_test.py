
# tests

assert f(0) == 7
assert f(1) == 11


# autres tests

for x in range(-10, 20):
    attendu = 4*x + 7
    assert f(x) == attendu, f"Erreur avec {x=}"
