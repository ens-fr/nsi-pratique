# tests

assert aire_trapeze(3, 1, 2) == 4  # Exemple 1
assert aire_trapeze(3, 5, 2) == 8  # Exemple 2
assert aire_trapeze(3, 3, 2) == 6  # Exemple 3





# autres tests

def sont_proches(x, y):
    return abs(x - y) < 10**-9

for base_1 in range(0, 20):
    for base_2 in range(0, 20):
        for hauteur in range(20):
            attendu = (base_1 + base_2) * hauteur / 2
            assert sont_proches(aire_trapeze(base_1, base_2, hauteur), attendu), f"Erreur avec {base_1=}, {base_2=} et {hauteur=}"
