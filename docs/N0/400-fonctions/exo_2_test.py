
# tests

assert g(0) == 1
assert g(1) == 4


# autres tests

for x in range(-10, 20):
    attendu = (x + 1)**2
    assert g(x) == attendu, f"Erreur avec {x=}"
