def Celsius_depuis_Fahrenheit(t_fahrenheit):
    t_celcius = (t_fahrenheit - 32) * 5 / 9
    return t_celcius
