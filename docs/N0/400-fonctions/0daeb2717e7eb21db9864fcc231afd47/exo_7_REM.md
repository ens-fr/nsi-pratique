## Commentaires

{{ IDE('exo_corr') }}

:warning: La réponse est simple, mais il faut évoquer un point

!!! failure "Pas d'égalité entre flottant"
    Dans les tests, nous avons écrit

    ```python
    assert circonference(0) == 0
    assert circonference(1) == 6.2831852
    ```

    Cette méthode peut être source d'erreurs, il ne faut pas faire de tests d'égalité entre flottants.

!!! success "On fait un test de nombres proches"

    ```python
    def sont_proches(x, y):
        return abs(x - y) < 10**-9

    assert sont_proches(circonference(0), 0)
    assert sont_proches(circonference(1), 6.2831852)
    ```

    Voilà une bonne méthode.

Les tests cachés sur cet exercice utilisaient la fonction `sont_proches` !

Comme dans tous les exercices où interviennent des tests d'égalité entre flottants.
