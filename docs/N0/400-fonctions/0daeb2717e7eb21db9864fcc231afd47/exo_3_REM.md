## Commentaires

{{ IDE('exo_corr') }}


### Variantes

```python
def f(x):
    # avec l'opérateur puissance
    y = 5*x**2 - 3*x + 1
    return y

# ou

def f(x):
    # remarquez qu'il n'y a que deux multiplications !
    y = (5*x-3) * x  +  1
    return y
```
