---
author: Jean Diraison, Franck Chambon
tags:
  - 0-simple
---

# Fonctions

## Fonction 0

Complétez la fonction suivante, sachant qu'elle traduit l'expression mathématique

$$f(x) = 4x + 7$$

{{ IDE('exo_0') }}


---

## Fonction 1

Vous devez créer une fonction qui prend en paramètre un nombre $x$ et qui renvoie $5x-3$.

{{ IDE('exo_1') }}


---

## Fonction 2

Complétez l'écriture de la fonction $g$ qui prend en paramètre un nombre $x$ et qui renvoie $(x+1)^2$.

{{ IDE('exo_2') }}

---

## Fonction 3

Complétez la fonction suivante, sachant qu'elle traduit l'expression mathématique

$$f(x) = 5x^2 -3x + 1$$

{{ IDE('exo_3') }}


---

## Fonction 4

Corrigez la fonction `produit` pour qu'elle prenne deux paramètres `k` et `n`, et renvoie leur produit.

{{ IDE('exo_4') }}


---

## Fonction 5

!!! info "Aire d'un trapèze"
    ![](./exo_5_trapeze_0.svg){ .autolight align=left }
    Un trapèze est un quadrilatère non croisé ayant deux côtés parallèles ; ses bases. Son aire est égale à la moyenne des bases, multipliée par la hauteur associée.

    $$\mathscr A_\text{trapèze} = \frac{b_1 + b_2}2 × h$$

Complétez la fonction `aire_trapeze` qui renvoie l'aire d'un trapèze.

Elle a pour paramètres :

- `base_1` : la longueur d'une base
- `base_2` : la longueur de l'autre base
- `hauteur` : la hauteur associée

{{ IDE('exo_5') }}


!!! example "Exemples de trapèzes"
    ![](./exo_5_trapeze_1.svg){ .autolight } ![](./exo_5_trapeze_2.svg){ .autolight } ![](./exo_5_trapeze_3.svg){ .autolight }

---

## Fonction 6

Créez la fonction `perimetre` de paramètre `L` et `l` qui calcule et renvoie le périmètre du rectangle de longueur `L` et de largeur `l`.

{{ IDE('exo_6') }}


---

## Fonction 7

Créez la fonction `circonference` de paramètre `rayon` qui renvoie le périmètre du cercle de rayon donné.

> On définira la constante `PI` de valeur `3.1415926` dans le script.

{{ IDE('exo_7') }}


---

## Fonction 8

Complétez la fonction `Celsius_depuis_Fahrenheit` qui permet de convertir une température donnée en degré Fahrenheit en une température en degré Celsius.

Aujourd'hui, l'échelle Fahrenheit est calée sur l'échelle Celsius par la relation : 

$$T(°F) = \frac95 T(°C) + 32$$

{{ IDE('exo_8') }}


---

## Fonction 9

Un parc d'attractions affiche les tarifs suivants :

- 8,50 € par enfant
- 12,00 € par adulte

Vous devez écrire une fonction `prix` qui renvoie le prix total à payer, à partir du nombre `n` d'enfants et du nombre `p` d'adultes.



{{ IDE('exo_9') }}

