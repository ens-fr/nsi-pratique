def f(x):
    y = ...        # ligne à modifier
    return y


# tests

assert f(0) == 1
assert f(1) == 3
assert f(-1) == 9

