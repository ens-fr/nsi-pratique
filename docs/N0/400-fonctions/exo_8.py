def Celsius_depuis_Fahrenheit(t_fahrenheit):
    t_celcius = ...        # ligne à modifier
    return t_celcius


# tests

def sont_proches(x, y):
    return abs(x - y) < 10**-9

assert sont_proches(Celsius_depuis_Fahrenheit(-40), -40)
assert sont_proches(Celsius_depuis_Fahrenheit(+32),   0)
assert sont_proches(Celsius_depuis_Fahrenheit(+50),  10)
assert sont_proches(Celsius_depuis_Fahrenheit(212), 100)

