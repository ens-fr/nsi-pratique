# tests

def sont_proches(x, y):
    return abs(x - y) < 10**-9

assert sont_proches(Celsius_depuis_Fahrenheit(-40), -40)
assert sont_proches(Celsius_depuis_Fahrenheit(+32),   0)
assert sont_proches(Celsius_depuis_Fahrenheit(+50),  10)
assert sont_proches(Celsius_depuis_Fahrenheit(212), 100)


# autres tests

for t_fahrenheit in range(-10, 20):
    attendu = (t_fahrenheit - 32) / 1.8
    assert sont_proches(Celsius_depuis_Fahrenheit(t_fahrenheit), attendu), f"Erreur avec {t_fahrenheit=}"
