def aire_trapeze():
    ...


# tests

assert aire_trapeze(3, 1, 2) == 4  # Exemple 1
assert aire_trapeze(3, 5, 2) == 8  # Exemple 2
assert aire_trapeze(3, 3, 2) == 6  # Exemple 3

