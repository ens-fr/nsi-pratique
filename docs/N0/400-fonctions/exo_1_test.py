
# tests

assert f(0) == -3
assert f(1) == 2


# autres tests

for x in range(-10, 20):
    attendu = 5*x - 3
    assert f(x) == attendu, f"Erreur avec {x=}"
