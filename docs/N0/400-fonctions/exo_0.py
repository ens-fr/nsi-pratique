def f(x):
    y = ...        # ligne à modifier
    return y


# tests

assert f(0) == 7
assert f(1) == 11
