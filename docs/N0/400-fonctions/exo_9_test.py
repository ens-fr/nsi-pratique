
# tests

assert prix(0, 0) == 0.0
assert prix(1, 0) == 8.5
assert prix(0, 1) == 12.0



# autres tests


def sont_proches(x, y):
    return abs(x - y) < 10**-9

for n in range(20):
    for p in range(20):
        attendu = n * 8.5  +  p * 12.0
        assert sont_proches(prix(n, p), attendu), f"Erreur avec {n=} et {p=}"
