
# tests

assert circonference(0) == 0
assert circonference(1) == 6.2831852


# autres tests


def sont_proches(x, y):
    return abs(x - y) < 10**-9

for r in range(20):
    attendu = 2 * 3.1415926 * r 
    assert sont_proches(circonference(r), attendu), f"Erreur avec {r=}"
