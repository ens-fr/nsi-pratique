## Commentaires

{{ IDE('exo_corr') }}

### Solution alternative

On pourrait aussi utiliser les listes en compréhension :

```python
def partage(valeurs, n):
    taille = len(valeurs)
    return (
        [valeurs[i] for i in range(n)],
        [valeurs[i] for i in range(n, taille)],
    )
```

### Utilisation des tranches

**On ne recommande pas les tranches en NSI**, mais on peut obtenir une solution simple avec :

```python
def partage(valeurs, n):
    return valeurs[:n], valeurs[n:]
```

En effet :

- `valeurs[a:b]` renvoie une **copie** de `valeurs` de l'indice `a` inclus jusqu'à l'indice `b` exclu ;
- omettre `a` signifie partir du début ;
- omettre `b` signifie aller jusqu'à la fin.

On peut aussi utiliser des indices négatifs, par exemple :

- `valeurs[-5:]` renvoie les 5 derniers éléments ;
- `valeurs[:-5]` renvoie tout sauf les 5 derniers éléments.
