# Cet automate reconnaît "bato" et "baato" mais pas "bota" (exemple du sujet)
automate_0 = {
    (0, "b"): 1,
    (1, "a"): 2,
    (2, "a"): 2,
    (2, "t"): 3,
    (3, "o"): 4,
    (4, "o"): 4,
}
debut_0 = 0
fin_0 = 4

# Cet automate reconnaît "tada" et "tadaaa" mais pas "tad" ni "taada"
automate_1 = {(0, "t"): 1, (1, "a"): 2, (2, "d"): 3, (3, "a"): 4, (4, "a"): 4}
debut_1 = 0
fin_1 = 4

# Cet automate reconnaît "ruche" et "riche" mais pas "reche" ni "rche"
automate_2 = {
    (0, "r"): 1,
    (1, "u"): 2,
    (2, "c"): 4,
    (1, "i"): 3,
    (3, "c"): 4,
    (4, "h"): 5,
    (5, "e"): 6,
}
debut_2 = 0
fin_2 = 6

# Cet automate reconnaît `"vroum"`, `"broum"` et `"vrououm"` mais pas `"brou"` ni `"vrouom"`
automate_3 = {
    (0, "v"): 1,
    (0, "b"): 2,
    (1, "r"): 3,
    (2, "r"): 3,
    (3, "o"): 4,
    (4, "u"): 5,
    (5, "o"): 4,
    (5, "m"): 6,
}
debut_3 = 0
fin_3 = 6
