# Tests
xA, yA = 2, -5
xB, yB = -3, 1
assert manhattan(xA, yA, xB, yB) == 11
assert euclidienne_carre(xA, yA, xB, yB) == 61
assert tchebychev(xA, yA, xB, yB) == 6

xA, yA = 2, 2
xB, yB = 2, 2
assert manhattan(xA, yA, xB, yB) == 0
assert euclidienne_carre(xA, yA, xB, yB) == 0
assert tchebychev(xA, yA, xB, yB) == 0

# Tests supplémentaires
points = [
    (2, 3),
    (10, -3),
    (0, -5),
    (3, -6),
    (-1, -10),
    (-10, -1),
    (-10, -3),
    (5, 1),
    (0, 0),
    (-5, 3),
]
for i in range(len(points) - 1):
    xA, yA = points[i]
    xB, yB = points[i + 1]
    attendu = abs(xB - xA) + abs(yB - yA)
    assert manhattan(xA, yA, xB, yB) == attendu, f"Erreur sur le calcul de manhattan{xA, yA, xB, yB}"
    attendu = (xB - xA) ** 2 + (yB - yA) ** 2
    assert euclidienne_carre(xA, yA, xB, yB) == attendu, f"Erreur sur le calcul de euclidienne_carre{xA, yA, xB, yB}"
    attendu = max(abs(xB - xA), abs(yB - yA))
    assert tchebychev(xA, yA, xB, yB) == attendu, f"Erreur sur le calcul de tchebychev{xA, yA, xB, yB}"
    xA, yA = points[i + 1]
    xB, yB = points[i]
    attendu = abs(xB - xA) + abs(yB - yA)
    assert manhattan(xA, yA, xB, yB) == attendu, f"Erreur sur le calcul de manhattan{xA, yA, xB, yB}"
    attendu = (xB - xA) ** 2 + (yB - yA) ** 2
    assert euclidienne_carre(xA, yA, xB, yB) == attendu, f"Erreur sur le calcul de euclidienne_carre{xA, yA, xB, yB}"
    attendu = max(abs(xB - xA), abs(yB - yA))
    assert tchebychev(xA, yA, xB, yB) == attendu, f"Erreur sur le calcul de tchebychev{xA, yA, xB, yB}"