---
author: Nicolas Revéret
title: Différentes distances
tags:
    - 0-simple
    - 4-maths
    - 4-fonctions
---

# Différentes distances

Il existe en mathématiques et en informatique de nombreuses façons de calculer la distance entre deux points.

![Points A et B](points.svg){width=40%}

On étudie ici les trois distances suivantes :

=== "Distance de Manhattan"

    C'est la distance parcourue « en suivant les rues » dans le quartier de Manhattan. Elle vaut $12$ dans la figure ci-dessous, quel que soit le chemin suivi :

    ![Distance de Manhattan](Manhattan_distance.svg){width=30%}

    On la définit comme la somme des « écarts » entre chaque paires de coordonnées des points :

    $\mathrm{manhattan}(x_A, y_A, x_B, y_B) = \lvert x_B - x_A \rvert + \lvert y_B - y_A \rvert$

    !!! tip "Aide"

        Avec Python, on calcule la valeur absolue de $a$ en faisant `#!py abs(a)`.

=== "Distance euclidienne"

    C'est la distance classique du cours de mathématiques (dans le cas où les axes sont perpendiculaires et de même échelle).

    ![Distance euclidienne](Euclidean_distance.svg){width=40%}

    On la calcule à l'aide du théorème de Pythagore :

    $\mathrm{euclidienne}(x_A, y_A, x_B, y_B) = \sqrt{(x_B - x_A)^2+(y_B - y_A)^2}$

    Afin de simplifier les calculs, on utilisera ici le carré de la distance euclidienne :

    $\mathrm{euclidienne\_carrée}(x_A, y_A, x_B, y_B) = (x_B - x_A)^2+(y_B - y_A)^2$

    !!! tip "Aide"

        Avec Python, on calcule le carré de $a$ en faisant `#!py a**2`.

=== "Distance de Tchebytchev"

    Aux échecs, le roi peut se déplacer d'une case à la fois dans toutes les directions.

    Combien de coups lui faut-il pour atteindre une case donnée ? La réponse est donnée par la distance de Tchebytchev.

    Sur la figure ci-dessous, les cases marquées sont toutes accessibles en $2$ coups.

    ![Distance de Tchebytchev](Tchebytchev_distance.svg){width=30%}

    Cette distance est égale à l'« écart » maximal entre les coordonnées de chaque point :

    $\mathrm{tchebytchev}(x_A, y_A, x_B, y_B) = \max (\lvert x_B - x_A \rvert , \lvert y_B - y_A \rvert)$

    !!! tip "Aide"

        Avec Python, on calcule le maximum de deux nombres `a` et `b` en faisant `#!py max(a, b)`.

!!! example "Exemples"

    En prenant $A(2\,;\,-5)$ et $B(-3\,;\,1)$ :

    ```pycon
    >>> xA, yA = 2, -5
    >>> xB, yB = -3, 1
    >>> manhattan(xA, yA, xB, yB) 
    11
    >>> euclidienne_carre(xA, yA, xB, yB) 
    61
    >>> tchebychev(xA, yA, xB, yB)
    6
    ```
    
    En prenant $A(2\,;\,2)$ et $B(2\,;\,2)$ :

    ```pycon
    >>> xA, yA = 2, 2
    >>> xB, yB = 2, 2
    >>> manhattan(xA, yA, xB, yB)
    0
    >>> euclidienne_carre(xA, yA, xB, yB)
    0
    >>> tchebychev(xA, yA, xB, yB)
    0
    ```

Compléter les fonctions ci-dessous :

{{ IDE('exo') }}
