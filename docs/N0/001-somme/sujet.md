---
author: Franck Chambon
title: Somme de deux entiers
tags:
  - 0-simple
---
# Somme de deux entiers

Compléter la fonction ci-dessous pour qu'elle renvoie la somme des deux arguments `a` et `b`.

!!! example "Exemple"

    ```pycon
    >>> somme(10, 32)
    42
    >>> somme(100, 7)
    107
    ```

{{ IDE('exo') }}
