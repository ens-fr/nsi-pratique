# Exemple donné de traduction de condition
# prop_1 : "3 est strictement inférieur à 4"
prop_1 = 3 < 4

# prop_2 : "3 est supérieur ou égal à 4"
prop_2 = 3 >= 4

# prop_3 : "le caractère 'n' est dans la chaine 'bonjour'"
prop_3 = "n" in "bonjour"

# prop_4 : "Le calcul 3 + 5 * 2 vaut 16"
prop_4 = 3 + 5 * 2 == 16

# prop_5 : "5 est un nombre pair"
prop_5 = 5 % 2 == 0

# prop_6 : "12 est dans la liste [k for k in range(12)]"
prop_6 = 12 in [k for k in range(12)]

# prop_7 : "'ju' n'est pas dans 'bonjour'"
prop_7 = "ju" not in "bonjour"

# prop_8 : "3 est égal à 1 + 2 et 'a' est dans 'Boole'"
prop_8 = (3 == 1 + 2) and ("a" in "Boole")

# prop_9 : "3 est égal à 1 + 2 ou 'a' est dans 'Boole'"
prop_9 = (3 == 1 + 2) or ("a" in "Boole")

# prop_10 : "6 est un nombre pair et un multiple de 3"
prop_10 = (6 % 2 == 0) and (6 % 3 == 0)

# prop_11 : "648 est dans les tables de 2, de 3 et de 4"
prop_11 = ((648 % 2 == 0) and (648 % 3 == 0)) and (648 % 4 == 0)

# prop_12 : "25 est strictement compris entre 24 et 26"
prop_12 = (25 > 24) and (25 < 26)
# prop_12 : "ou"
prop_12 = 24 < 25 < 26

# prop_13 : "'a' est dans 'hello' ou dans 'hallo'"
prop_13 = ("a" in "hello") or ("a" in "hallo")

# prop_14 : "8 est égal à 4 * 2 et différent de 9 - 1"
prop_14 = (8 == 4 * 2) and (8 != 9 - 1)

# prop_15 : "7 est entre 1 et 5 (inclus l'un et l'autre) ou strictement supérieur à 6"
prop_15 = (7 >= 1) and (7 <= 5) or (7 >= 6)
# prop_15 : "ou"
prop_15 = (1 <= 7 <= 5) or (7 >= 6)

# prop_16: "5 est strictement compris entre 7 ou 8 est strictement inférieur à 9"
prop_16 = (7 < 5 and 5 < 8) or 8 < 9

# prop_17: "7 est strictement inférieur à 5 et ...
# ...(8 est strictement supérieur à 5 ou strictment inférieur à 9)"
prop_17 = 7 < 5 and (5 < 8 or 8 < 9)
