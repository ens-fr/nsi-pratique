# Tests
assert prop_1, "Erreur avec : 3 est strictement inférieur à 4"
assert not prop_2, "Erreur avec : 3 est strictement inférieur à 4"
assert prop_3, "Erreur avec : le caractère 'n' est dans la chaine 'bonjour'"
assert not prop_4, "Erreur avec : Le calcul 3 + 5 * 2 vaut 16"
assert not prop_5, "Erreur avec : 5 est un nombre pair"
assert not prop_6, "Erreur avec : 12 est dans la liste [k for k in range(12)]"
assert prop_7, "Erreur avec : 'ju' n'est pas dans 'bonjour'"
assert not prop_8, "Erreur avec : 3 est égal à 1 + 2 et 'a' est dans 'Boole'"
assert prop_9, "Erreur avec : 3 est égal à 1 + 2 ou 'a' est dans 'Boole'"
assert prop_10, "Erreur avec : 6 est un nombre pair et un multiple de 3"
assert prop_11, "Erreur avec : 648 est dans la table de 2, de 3 et de 4"
assert prop_12, "Erreur avec : 25 est compris entre 24 et 26 (au sens strict)"
assert prop_13, "Erreur avec : 'a' est dans 'hello' ou dans 'hallo'"
assert not prop_14, "Erreur avec : 8 est égal à 4 * 2 et différent de 9 - 1"
assert (
    prop_15
), "Erreur avec : 7 est compris entre 1 et 5 (au sens large) ou strictement supérieur à 6"
assert (
    prop_16
), "Erreur avec : 5 est strictement compris entre 7 et 8 ou 8 est strictement inférieur à 9"
assert not (
    prop_17
), "Erreur avec : 7 est strictement inférieur à 5 et (8 est strictement supérieur à 5 ou strictment inférieur à 9)"
