def non(a):
    if a:
        return False
    else:
        return True


def et(a, b):
    if a:
        if b:
            return True
    return False


def ou(a, b):
    if a:
        return True
    elif b:
        return True
    return False


def ou_exclusif(a, b):
    return ou(et(a, non(b)), et(non(a), b))


def non_ou(a, b):
    return non(ou(a, b))


def non_et(a, b):
    return non(et(a, b))


# Tests
assert non(True) == False
assert et(True, False) == False
assert ou(True, False) == True
assert ou_exclusif(True, True) == False
assert non_ou(False, True) == False
assert non_et(False, True) == True
