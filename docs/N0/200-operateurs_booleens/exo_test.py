# Tests
assert non(True) == False
assert et(True, False) == False
assert ou(True, False) == True
assert ou_exclusif(True, True) == False
assert non_ou(False, True) == False
assert non_et(False, True) == True

# Test supplémentaires
for a, b in ((False, False), (False, True), (True, False), (True, True)):
    assert non(a) == (not a)
    assert et(a, b) == (a and b)
    assert ou(a, b) == (a or b)
    assert ou_exclusif(a, b) == (a ^ b)
    assert non_ou(a, b) == (not (a or b))
    assert non_et(a, b) == (not (a and b))
