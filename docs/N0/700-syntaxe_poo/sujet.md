---
author: Nicolas Revéret
title: Syntaxe de la POO
tags:
  - 7-POO
---

# `programmeur.saisit(code)`

On travaille dans cet exercice sur deux **classes** :

* la classe `Chose` permet de décrire une chose (objet, élément du décor...) dans le jeu.
* la classe `Personnage` permet de représenter un personnage d'un jeu vidéo,

Une `Chose` est caractérisée par un unique **attribut**, son `genre` de type `str`. Ce genre doit impérativement appartenir à la liste `["livre", "pioche", "pierre", "gateau", "diamant"]`.

```mermaid
classDiagram
class Chose{
       genre : str
}
```

Un `Personnage` est caractérisé par des **attributs** :

* un `nom`(au format `str`),
* une `energie` (au format `int`, compris entre `0` et `500`),
* un `inventaire` (au format `list`).

Lors de la création d'un objet type `Personnage` on passe en arguments son nom puis son énergie : `bruce = Personnage('Batman', 450)`. L'inventaire est créé automatiquement et vide au début.

Il est possible d'appliquer des **méthodes** à un `Personnage`. Si `perso` est un objet de la classe `Personnage` :

* `prend` : `perso` prend un `truc` (de type `Chose`),
* `mange` : `perso` mange un `truc` (de type `Chose`),
* `parle` : `perso` parle avec une autre `personne` (de type `Personnage`),
* `donne` : `perso` donne est possible, un `truc` à une autre `personne` (respectivement de types `Chose` et `Personnage`).

!!! note "Remarque"

       La documentation officielle de Python utilise un vocabulaire différent : les attributs et les méthodes d'un objet sont tous dénommés *attributs*. La documentation distingue alors *attributs __données__* et *méthodes*.

Toutes ces méthodes renvoient des chaînes de caractères décrivant le résultat de l'action.

Certaines actions sont impossibles. Par exemple `perso` ne peut pas manger un `truc` du genre `"diamant"`.

```mermaid
classDiagram
class Personnage{
       nom : str
       energie : int
       inventaire : list
       prend(truc : Chose) str
       mange(truc : Chose) str
       parle(personne : Personnage) str
       donne(truc : Chose, personne : Personnage) str
}
```

!!! note "Remarque"

       Dans le diagramme précédent le texte `prend(truc : Chose) str` signifie que la classe `Personnage` possède une méthode `prend` prenant un argument nommé `truc` de type `Chose` et qui renvoie une chaîne de caractère (`str`).


???+ tip "Syntaxe"

    Il est possible de créer une `Chose` en saisissant l'instruction `truc = Chose("pierre")`.

    De plus, si `untel` et `unetelle` sont des objets de type `Personnage` et `truc` un objet de type `Chose`, il est par exemple possible :

    * d'accéder à la valeur d'un attribut en saisissant `inv = untel.inventaire`,
  
    * de modifier la valeur d'un attribut en saisissant `unetelle.energie = 125`,
      
    * d'utiliser une méthode en saisissant `untel.donne(truc, unetelle)`.

Compléter le code ci-dessous avec les instructions valides :

{{ IDE('exo') }}