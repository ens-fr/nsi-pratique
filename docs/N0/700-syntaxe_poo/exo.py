#--- HDR ---#
GENRES_OBJETS = {
    "livre": {"utilisable", "peut être pris"},
    "pioche": {"utilisable", "peut être pris"},
    "pierre": {"cassable", "utilisable"},
    "gateau": {"comestible", "peut être pris"},
    "diamant": {"peut être pris"},
}


class Personnage:
    def __init__(self, nom, energie):
        self._nom = nom
        self._energie = energie
        self._inventaire = []

    @property
    def nom(self):
        return self._nom

    @property
    def energie(self):
        return self._energie

    @energie.setter
    def energie(self, valeur):
        if not 0 <= valeur <= 500:
            return "L'énergie est toujours comprise entre 0 et 500"
        self._energie = valeur

    @property
    def inventaire(self):
        return self._inventaire

    def prend(self, truc):
        if not isinstance(truc, Chose):
            return f"{self._nom} ne peut pas prendre un(e) {truc}"
        if not ("peut être pris" in GENRES_OBJETS[truc.genre]):
            return f"{self._nom} ne peut pas prendre un(e) {truc}"
        if (truc in self.inventaire):
            return f"{truc} est déjà dans l'inventaire"
        self._inventaire.append(truc)
        return f"{self.nom} prend un(e) {truc}"

    def mange(self, truc):
        if truc not in self._inventaire:
            return f"{truc} n'est pas dans l'inventaire"
        if not ("comestible" in GENRES_OBJETS[truc.genre]):
            return f"{self._nom} ne peut pas manger {truc} !"
        self._inventaire.remove(truc)
        return f"{self.nom} mange un(e) {self.truc}"

    def parle(self, personne):
        if not isinstance(personne, Personnage):
            return f"{self._nom} ne peut pas parler à {personne}"
        return f"{self._nom} et {personne.nom} discutent"

    def donne(self, truc, personne):
        if not isinstance(personne, Personnage):
            return f"{self._nom} ne peut pas donner {truc} à {personne}"
        if not isinstance(truc, Chose):
            return f"{self._nom} ne peut pas donner {truc} à {personne}"
        if not ("peut être pris" in GENRES_OBJETS[truc.genre]):
            return f"{self._nom} ne peut pas donner un(e) {truc}"

        self._inventaire.remove(truc)
        personne._inventaire.append(truc)
        return f"{self.nom} donne un(e) {truc} à {personne}"

    def __str__(self):
        return f"{self._nom} ({self._energie}, {list(self._inventaire)})"

    def __repr__(self):
        return self.__str__()


class Chose:
    def __init__(self, genre):
        if not (genre.lower() in GENRES_OBJETS):
            return f"Le genre doit être dans {', '.join([g for g in GENRES_OBJETS])}"
        self._genre = genre.lower()

    @property
    def genre(self):
        return self._genre

    def __str__(self):
        return f"Un(e) {self._genre}"

    def __repr__(self):
        return self.__str__()


#--- HDR ---#


# Guybrush a 100 d'énergie
# Ce Personnage est affecté à la variable guy
... = ...
assert str(guy) == "Guybrush (100, [])"

# Helaine a 200 d'énergie
# Ce Personnage est affecté à la variable helaine
...
assert str(helaine) == "Helaine (200, [])"

# Faites parler Guybrush et Helaine
resultat = ...
assert resultat == "Guybrush et Helaine discutent"

# Faites parler Helaine et Guybrush
resultat = ...
assert resultat == "Helaine et Guybrush discutent"

# Affectez l'énergie de Guybrush à la variable 'nrj'
...
assert nrj == 100

# Faites en sorte que Guybrush ait 199 d'énergie
...
assert guy.energie == 199

# Créez une chose du genre diamant
# Affectez le à la variable diamant
...
assert diamant.genre == "diamant"

# Faites en sorte que Guybrush prenne le diamant
...
assert (diamant in guy.inventaire) == True

# Faites en sorte que Guybrush donne le diamant à Helaine
...
# Vérifiez à l'aide de deux assertions que :
#   - Guybrush ne possède plus le diamant
#   - Helaine possède le diamant
...
...