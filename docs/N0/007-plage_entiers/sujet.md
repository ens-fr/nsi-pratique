---
author: Nicolas Revéret
title: Domaine des entiers
tags:
    - 0-simple
status: relecture
---

# Valeurs minimales et maximales sur $n$ bits

Un ordinateur manipule des nombres écrits en binaire : uniquement avec les chiffres « $0$ » et « $1$ ».

Dans certains langages de programmation, il existe plusieurs types associés aux nombres entiers. Ces différents types permettent notamment :

* de faire varier le nombre de bits utilisés (et donc les valeurs extrêmes des nombres manipulés),

* de ne manipuler **que** des nombres positifs (entiers non signés) ou des positifs **et** des négatifs (entiers signés).

Voici par exemple quelques types d'entiers utilisés par le langage C :

|      type       | nombre de bits |       Valeur minimale        |       Valeur maximale        |
| :-------------: | :------------: | :--------------------------: | :--------------------------: |
|      `int`      |      $32$      |       $-2~147~483~648$       |       $2~147~483~647$        |
| `unsigned int`  |      $32$      |             $0$              |       $4~294~967~295$        |
|     `long`      |      $64$      | $-9~223~372~036~854~775~808$ | $9~223~372~036~854~775~807$  |
| `unsigned long` |      $64$      |             $0$              | $18~446~744~073~709~551~615$ |

Comme on peut le voir, les entiers signés s'étendent sur des valeurs négatives et positives, mais leur valeur maximale est inférieure à celle de l'entier non signé utilisant autant de bits.

Les formules permettant de calculer ces valeurs extrêmes sont les suivantes :

|       type       | nombre de bits | Valeur minimale | Valeur maximale |
| :--------------: | :------------: | :-------------: | :-------------: |
|   entier signé   |      $n$       |   $-2^{n-1}$    |   $2^{n-1}-1$   |
| entier non signé |      $n$       |       $0$       |     $2^n-1$     |

Par exemple, pour un entier signé sur $8$ bits on obtient :

* Valeur minimale : $-2^{8-1}=-2^7=-128$,
  
* Valeur maximale : $2^{8-1} - 1=2^7 - 1=127$.

Écrire la fonction `plage` qui prend en argument le nombre de bits utilisés `n` (entier strictement positif) ainsi qu'un booléen `signe` valant `True` si l'on considère des entiers signés et `False` pour les non signés et qui renvoie les valeurs minimales et maximales correspondantes.

???+ tip "Astuce"

    Une fonction peut renvoyer plusieurs valeurs en les séparant par des virgules. Par exemple la fonction ci-dessous renvoie toujours `(0, 0, 7)` :

    ```python
    def numero_espion():
        return (0, 0, 7)
    ```

    Techniquement, cette fonction renvoie un *tuple*.

!!! example "Exemples"

    ```pycon
    >>> plage(1, False)
    (0, 1)
    >>> plage(1, True)
    (-1, 0)
    >>> plage(8, False)
    (0, 255)
    >>> plage(8, True)
    (-128, 127)
    ```

{{ IDE('exo') }}
