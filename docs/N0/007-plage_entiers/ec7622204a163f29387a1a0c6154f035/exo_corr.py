def plage(n, signe):
    if signe:
        limite = 2 ** (n - 1)
        return -limite, limite - 1
    else:
        return 0, 2**n - 1


# Tests
assert plage(1, False) == (0, 1)
assert plage(1, True) == (-1, 0)
assert plage(8, False) == (0, 255)
assert plage(8, True) == (-128, 127)
