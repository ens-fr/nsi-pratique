# 🗒️ Présentation

Les exercices pour démarrer facilement. Ils sont guidés et sont des applications directes du cours.

![](Cuneiform_tablet-_fragment_of_a_mathematical_problem_text_MET_ME86_11_404.jpg)

> [Fragment d'une tablette cunéiforme](https://fr.wikipedia.org/wiki/Algorithmique#Antiquit%C3%A9) avec un problème algorithmique.
