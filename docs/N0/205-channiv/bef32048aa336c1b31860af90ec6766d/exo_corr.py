def channiv(age, num_vie, lang="fr"):
    if lang == "fr":
        carte = "Joyeux "
        carte += str(age)
        if age == 1:
            carte += "er"
        else:
            carte += "e"
        carte += " channiversaire de ta "
        carte += str(num_vie)
        if num_vie == 1:
            carte += "re"
        else:
            carte += "e"
        carte += " vie"
        return carte
    elif lang == "en":
        carte = "Happy purrthday, for "
        carte += str(age)
        if age == 1:
            carte += "st"
        elif age == 2:
            carte += "nd"
        elif age == 3:
            carte += "rd"
        else:
            carte += "th"
        carte += " year of "
        carte += str(num_vie)
        if num_vie == 1:
            carte += "st"
        elif num_vie == 2:
            carte += "nd"
        elif num_vie == 3:
            carte += "rd"
        else:
            carte += "th"
        carte += " life"
        return carte
    else:
        return "Je donne ma langue au chat"
