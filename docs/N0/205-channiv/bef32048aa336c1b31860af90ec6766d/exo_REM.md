## Commentaires

Une solution assez directe, mais non factorisée

{{ IDE('exo_corr') }}

### Avec un code plus factorisé

```python
def ordinal(n, lang, genre=None):
    """Renvoie l'ordinal de n dans la lang, accordé en genre"""
    if lang == "fr":
        if n == 1:
            assert genre is not None, "Il faut préciser le genre en français"
            if genre == "féminin":
                return "1re"
            if genre == "masculin":
                return "1er"
        else:
            return str(n) + "e"
    elif lang == "en":
        if n == 1:
            return "1st"
        elif n == 2:
            return "2nd"
        elif n == 3:
            return "3rd"
        else:
            return str(n) + "th"

def channiv(age, num_vie, lang="fr"):
    if lang == "fr":
        carte = "Joyeux "
        carte += ordinal(age, "fr", "masculin")
        carte += " channiversaire de ta "
        carte += ordinal(num_vie, "fr", "féminin")
        carte += " vie"
        return carte
    elif lang == "en":
        carte = "Happy purrthday, for "
        carte += ordinal(age, "en")
        carte += " year of "
        carte += ordinal(num_vie, "en")
        carte += " life"
        return carte
    else:
        return "Je donne ma langue au chat"
```
