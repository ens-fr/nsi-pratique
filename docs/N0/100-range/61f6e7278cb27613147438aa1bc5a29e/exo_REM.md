## Commentaires

{{ IDE('exo_corr') }}


L'instruction `#!py range` permet en Python de générer les nombres au fur et à mesure, sans les stocker. 

La conversion en `#!py list` à l'aide de `#!py list(range(1, 5))` permet donc de sauvegarder et visualiser l'ensemble des valeurs parcourues.

Une autre façon de générer ces listes de nombres, plus proche des usages Python est d'utiliser les listes en compréhension.

Ainsi, les instructions `#!py list(range(1, 5))` et `[k for k in range(1, 5))]` renvoient l'une et l'autre `[1, 2, 3, 4]`.