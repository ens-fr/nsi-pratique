---
author: Nicolas Revéret
title: Autour de range
tags:
  - 0-simple
  - 1-boucle
---
# Autour de `#!py range`

En Python, l'instruction `#!python range` permet de parcourir des nombres entiers. Cette fonction renvoie un objet **qui n'est pas une liste** et génère les nombres au fur et à mesure de leur utilisation.

Elle peut être utilisée avec **un**, **deux** ou **trois** paramètres.

Quelle que soit la méthode utilisée, ces paramètres sont toujours des **nombres entiers** (positifs ou négatifs).

## **Un** paramètre : `#!py range(b)`

L'appel `#!py range(b)` permet de parcourir les entiers de `0` (inclus) à `b` (exclu).

```pycon
>>> list(range(5))
[0, 1, 2, 3, 4]
>>> list(range(6))
[0, 1, 2, 3, 4, 5]
```

On note que dans chaque appel, la valeur de `b` est bien exclue du parcours.

!!! note "Remarque"

    La conversion en une liste Python (avec `#!py list(range(5))`) permet de visualiser les nombres parcourus par `#!py range`.

## **Deux** paramètres : `#!py range(a, b)`

L'appel `#!py range(a, b)` permet de parcourir les entiers de `a` (inclus) à `b` (exclu).

```pycon
>>> list(range(0, 5))
[0, 1, 2, 3, 4]
>>> list(range(-5, 2))
[-5, -4, -3, -2, -1, 0, 1]
```

!!! note "Remarque"

    Dans l'utilisation avec un seul paramètre, Python utilise par défaut la valeur `0` pour `a`.

## **Trois** paramètres : `#!py range(a, b, c)`

L'appel `#!py range(a, b, c)` permet de parcourir les entiers de `a` (inclus) à `b` (exclu) avec un pas (un écart) de `c`.

Ainsi, `#!python range(3, 18, 5)` permet de parcourir successivement les nombres `3`, `8` et `13`. En effet, :

* `3` est la valeur de départ (incluse),
* `8` la suit. En effet le pas vaut `5` et `3 + 5` égale `8`,
* `13` est égal à `8 + 5`.

Le `18` (égal à `13 + 5`) est bien exclu.

```pycon
>>> list(range(3, 18, 5))
[3, 8, 13]
>>> list(range(3, 19, 5))
[3, 8, 13, 18]
>>> list(range(20, 15, -1))
[20, 19, 18, 17, 16]
```

!!! note "Remarque"

    Dans les utilisations avec un ou deux paramètres, Python utilise par défaut la valeur `1` pour `c`.
 

Compléter le code ci-dessous.

{{ IDE('exo') }}
