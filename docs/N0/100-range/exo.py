# UN SEUL PARAMETRE #

# Les entiers de 0 (inclus) à 11 (exclu)
liste_1 = list(range(...))

# Les entiers de 0 (inclus) à 20 (exclu)
liste_2 = list(range(...))

# Les entiers de 0 (inclus) à 20 (inclus)
liste_3 = list(range(...))

# DEUX PARAMETRES #

# Les entiers de 3 (inclus) à 10 (exclu)
liste_4 = list(range(..., ...))

# Les entiers de 12 (inclus) à 20 (inclus)
liste_5 = list(range(..., ...))

# Les entiers de -10 (inclus) à 1 (exclu)
liste_6 = list(range(..., ...))

# Les entiers de -10 (inclus) à 1 (inclus)
liste_7 = list(range(..., ...))

# TROIS PARAMETRES #

# Les entiers de 20 (inclus) à 0 (exclu)
liste_8 = list(range(..., ..., ...))

# Les entiers de 500 (inclus) à 200 (exclu)
liste_9 = list(range(..., ..., ...))

# Les entiers de 10 (inclus) à -10 (exclu)
liste_10 = list(range(..., ..., ...))

# Les entiers de 25 (inclus) à -25 (inclus)
liste_11 = list(range(..., ..., ...))

# Les pairs de 1 (inclus) à 100 (exclu)
liste_12 = list(range(..., ..., ...))

# Les multiples de 3 de 0 (inclus) à 301 (exclu)
liste_13 = list(range(..., ..., ...))

# Les multiples de 7 de 0 (inclus) à 994 (inclus)
liste_14 = list(range(..., ..., ...))

# Les pairs de -100 (inclus) à 100 (exclu)
liste_15 = list(range(..., ..., ...))

# Les multiples de 3 de 300 (inclus) à 102 (exclu)
liste_16 = list(range(..., ..., ...))

# Les multiples de 13 de 3445 (inclus) à -559 (inclus)
liste_17 = list(range(..., ..., ...))
