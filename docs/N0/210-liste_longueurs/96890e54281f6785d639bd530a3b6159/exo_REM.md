## Commentaires

{{ IDE('exo_corr') }}

On pourrait aussi utiliser une liste en compréhension :

```python
def filtre_par_longueur(liste_prenoms, longueur):
    return [prenom for prenom in liste_prenoms if len(prenom) == longueur]
```

Il est aussi possible d'utiliser un style fonctionnel

```python
def filtre_par_longueur(liste_prenoms, longueur):
    def valide(prenom):
        return len(prenom) == longueur
    
    return list(filter(valide, liste_prenoms))
```
