---
author: Charles Poulmaire, Franck Chambon
title: Prénoms de longueur donnée
tags:
  - 0-simple
  - 1-boucle
  - 2-string
---

# Filtre par longueur

On dispose d'une liste de prénoms, et on s'intéresse à ceux d'une longueur donnée.

Compléter la fonction `filtre_par_longueur` afin qu'elle renvoie la liste des prénoms de la longueur souhaitée.

Cette fonction :

* prend deux arguments : `liste_prenoms`, une liste de prénoms et `longueur`, un entier naturel ;
* renvoie la liste des prénoms de `liste_prenoms` dont la longueur est égale à `longueur`.

On prendra soin de renvoyer les prénoms dans **leur ordre d'apparition** dans la liste initiale.

!!! example "Exemples"

    ```pycon
    >>> filtre_par_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 7)
    ['Francky', 'Charles', 'Nicolas']
    >>> filtre_par_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 3)
    ['Léa']
    >>> filtre_par_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 10)
    []
    ```

{{ IDE('exo') }}
