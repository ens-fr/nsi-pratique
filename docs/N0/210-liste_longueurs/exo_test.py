# Tests
prenoms = ["Anne", "Francky", "Charles", "Léa", "Nicolas"]
assert filtre_par_longueur(prenoms, 7) == ["Francky", "Charles", "Nicolas"]
assert filtre_par_longueur(prenoms, 3) == ["Léa"]
assert filtre_par_longueur(prenoms, 10) == []

# Tests supplémentaires

autres = ["CAB", "AB", "ABC", "A" * 30, "B" * 30]
assert filtre_par_longueur(autres, 3) == ["CAB", "ABC"]
assert filtre_par_longueur(autres, 30) == ["A" * 30, "B" * 30]
