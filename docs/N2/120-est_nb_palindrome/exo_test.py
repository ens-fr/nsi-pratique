# tests

assert inverse_chaine('bac') == 'cab'

assert not est_palindrome('NSI')

assert est_palindrome('ISN-NSI')

assert not est_palindromique(214312)

assert est_palindromique(213312)


# autres tests


assert inverse_chaine('azerty') == 'ytreza'
assert inverse_chaine('azerty0') == '0ytreza'

assert not est_palindrome('AZERTY')
assert not est_palindrome('AZERTY0')

assert est_palindrome('ABCTCBA')
assert est_palindrome('ABCDDCBA')

assert not est_palindromique(132213)
assert not est_palindromique(1327213)

assert est_palindromique(122345543221)
assert est_palindromique(1223457543221)

