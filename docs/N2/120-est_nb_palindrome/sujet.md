---
author: Franck Chambon
title: Nombre est-il palindromique
tags:
  - 2-string
  - 4-fonctions
---
# Tester si un nombre est palindromique

Un mot palindrome peut se lire de la même façon de gauche à droite ou de droite à
gauche : `'esse'`, `'radar'`, et `'non'` sont des mots palindromes.

De même certains nombres sont eux-aussi palindromiques : $33$, $121$, $345543$.

L'objectif de cet exercice est d'obtenir un programme Python permettant de tester si un nombre est palindromique, quand sa représentation décimale est un palindrome.

Pour remplir cette tâche, on vous demande de compléter le code des trois fonctions ci-dessous sachant que la fonction `est_palindromique` s'appuiera sur la fonction `est_palindrome` qui elle-même s'appuiera sur la fonction `inverse_chaine`.

La fonction `inverse_chaine` inverse l'ordre des caractères d'une chaine de caractères `chaine` et renvoie la chaine inversée.

La fonction `est_palindrome` teste si une chaine de caractères `chaine` est un
palindrome. Elle renvoie `True` si c'est le cas et `False` sinon. Cette fonction s'appuie sur la fonction précédente.

La fonction `est_palindromique` teste si un nombre `nombre` est palindromique. Elle renvoie `True` si c'est le cas et `False` sinon. Cette fonction s'appuie sur la fonction précédente.

!!! example "Exemples"

    ```pycon
    >>> inverse_chaine('bac')
    'cab'
    >>> est_palindrome('NSI')
    False
    >>> est_palindrome('ISN-NSI')
    True
    >>> est_palindromique(214312)
    False
    >>> est_palindromique(213312)
    True
    ```

Compléter le code des trois fonctions ci-dessous.

{{ IDE('exo') }}
