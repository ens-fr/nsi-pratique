## Commentaires 

{{ IDE('exo_corr')}}

Sous ses allures pseudo-concrètes, le sujet permet d'implémenter une [liste chainée](https://fr.wikipedia.org/wiki/Liste_cha%C3%AEn%C3%A9e) (classe `Train`) dont les maillons sont les objets de la classe `Wagon`.



