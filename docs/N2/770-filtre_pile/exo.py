#--- HDR ---#

class Pile:
    "Classe à ne pas modifier"
    def __init__(self, valeurs=[]):
        """Initialise une pile
        - vide, si `valeurs` n'est pas fourni ;
        - remplie avec `valeurs` sinon.
            - Le sommet de la pile est à la fin de la liste.
        """
        self.valeurs = list(valeurs)
    
    def est_vide(self):
        "Renvoie un booléen : la pile est-elle vide ?"
        return self.valeurs == list()
    
    def __str__(self):
        "Affiche la pile, en indiquant le sommet"
        return "| " + " | ".join(map(str, self.valeurs)) + ' <- sommet'
    
    def depile(self):
        """
        - Si la pile est vide, provoque une erreur.
        - Sinon, dépile un élément au sommet et le renvoie.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def empile(self, element):
        "Empile un élément au sommet de la pile"
        self.valeurs.append(element)
    
#--- HDR ---#
# la classe Pile est utilisable directement pour cet exercice

def filtre_positifs(donnees):
    "fonction à compléter"
    auxiliaire = Pile()
    while not ... .est_vide():
        element = donnees. ... ()
        auxiliaire. ...(...)
    
    resultat = Pile()
    while ...:
        element = ...
        donnees.empile(element)
        if element >= 0:
            ...
    
    return ...





# tests

donnees = Pile([4, -11, 7, -3, -1, 0, 6])
assert str(filtre_positifs(donnees)) == '| 4 | 7 | 0 | 6 <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| 4 | -11 | 7 | -3 | -1 | 0 | 6 <- sommet'

donnees = Pile([1, 2, 3, 4])
assert str(filtre_positifs(donnees)) == '| 1 | 2 | 3 | 4 <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| 1 | 2 | 3 | 4 <- sommet'

donnees = Pile([-4, -3, -2, -1])
assert str(filtre_positifs(donnees)) == '|  <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| -4 | -3 | -2 | -1 <- sommet'
