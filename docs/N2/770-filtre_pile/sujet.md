---
author: Franck Chambon
title: Filtre sur une pile
tags:
  - 7-pile
  - 7-POO
---

# Filtre des nombres positifs d'une pile

On donne une mise en œuvre simple de la structure de Pile avec les listes Python.

```python
class Pile:
    "Classe à ne pas modifier"
    def __init__(self, valeurs=None):
        """Initialise une pile
        - vide, si `valeurs` n'est pas fourni ;
        - remplie avec `valeurs` sinon.
            - Le sommet de la pile est à la fin de la liste.
        """
        if valeurs is None:
            self.valeurs = []
        else:
            self.valeurs = valeurs
    
    def est_vide(self):
        "Renvoie un booléen : la pile est-elle vide ?"
        return self.valeurs == []
    
    def __str__(self):
        "Affiche la pile, en indiquant le sommet"
        return "| " + " | ".join(map(str, self.valeurs)) + ' <- sommet'
    
    def depile(self):
        """
        - Si la pile est vide, provoque une erreur.
        - Sinon, dépile un élément au sommet et le renvoie.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def empile(self, element):
        "Empile un élément au sommet de la pile"
        self.valeurs.append(element)
```

Pour cet exercice, on n'utilisera **que** les méthodes de la classe `Pile` et non les méthodes des listes Python. À savoir, uniquement :

- Avec `pile` qui est un objet de la classe `Pile`,
    - `pile.empile(element)` ; empile un `element` au sommet de `pile`,
    - `pile.depile()` ; dépile et renvoie `element` au sommet de `pile` qui est **non vide**.


Écrire une fonction `filtre_positif` qui prend en paramètre `donnees` de type `Pile` d'entiers et qui **renvoie une pile** qui contient les éléments positifs de `donnees`. La pile `donnees` doit être globalement inchangée.

!!! example "Exemples"

    ```pycon
    >>> donnees = Pile([4, -11, 7, -3, -1, 0, 6])
    >>> print(filtre_positifs(donnees))
    | 4 | 7 | 0 | 6 <- sommet
    >>> print(donnees)
    | 4 | -11 | 7 | -3 | -1 | 0 | 6 <- sommet
    ```

    ```pycon
    >>> donnees = Pile([1, 2, 3, 4])
    >>> print(filtre_positifs(donnees))
    | 1 | 2 | 3 | 4 <- sommet
    >>> print(donnees)
    | 1 | 2 | 3 | 4 <- sommet
    ```

    ```pycon
    >>> donnees = Pile([-4, -3, -2, -1])
    >>> print(filtre_positifs(donnees))
    |  <- sommet
    >>> print(donnees)
    | -4 | -3 | -2 | -1 <- sommet
    ```

{{ IDE('exo') }}
