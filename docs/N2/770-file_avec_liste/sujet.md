---
author: Romain Janvier
title: File avec liste
tags:
  - 7-file
  - 7-POO
---

# File à partir d'une liste chainée

On veut écrire une classe pour gérer une file à l'aide d'une liste chainée. On dispose d'une classe `Maillon` permettant la création d'un maillon de la chaine, celui-ci étant constitué d'une valeur et d'une référence au maillon suivant de la chaine (à sa création le maillon ne connait pas son suivant, d'où l'utilisation de l'objet `None`) :

```python
class Maillon:
    def __init__(self, valeur) :
        self.valeur = valeur
        self.suivant = None
```

La file non vide est constituée de maillons avec un maillon de `tete`, celui qu'on va enlever lors de l'action `defile` et un maillon de `fin` qui est le dernier à être arrivé par l'action `enfile`. S'il n'y a qu'un seul élément dans la file, le maillon de `tete` est le même que celui de `fin`.

Compléter la classe `File` et vérifier votre travail sur les exemples. On vous donne l'initialiseur, la méthode `est_vide` qui renvoie `True` si la file est vide et `False` sinon, ainsi que la méthode `affiche` qui permet de voir l'état de la file manipulée. 

!!! example "Exemples"

    ```pycon
    >>> file = File()
    >>> file.est_vide()
    True
    >>> file.enfile(2)
    >>> file.affiche()
    ← 2 ←
    >>> file.est_vide()
    False
    >>> file.enfile(5)
    >>> file.enfile(7)
    >>> file.affiche()
    ← 2 5 7 ←
    >>> file.defile()
    2
    >>> file.defile()
    5
    >>> file.affiche()
    ← 7 ←
    ```

{{ IDE('exo') }}


