class Maillon:
    def __init__(self, valeur):
        self.valeur = valeur
        self.suivant = None

class File:
    def __init__(self):
        self.tete = None
        self.fin = None

    def est_vide(self):
        return self.fin is None

    def affiche(self):
        maillon = self.tete
        print('←', end=' ')
        while maillon is not None:
            print(maillon.valeur, end=' ')
            maillon = maillon.suivant
        print('←')

    def enfile(self, element):
        nouveau_maillon = ...
        if self.est_vide():
            self.tete = ...
        else:
            self.fin.suivant = ...
        self... = nouveau_maillon

    def defile(self):
        if ...:
            resultat = ...
            self.tete = ...
            if self.tete is None:
                self.fin = ...
            return ...
        else:
            return None

# tests

file = File()
assert file.est_vide()
file.enfile(2)
assert not file.est_vide()
file.enfile(5)
file.enfile(7)
assert file.defile() == 2
assert file.defile() == 5
