---
author: Franck Chambon
title: Sortie de labyrinthe
tags:
  - 4-grille
  - 8-graphe
---

# Vérification de sortie de labyrinthe

![Labyrinthe de buisson](clipart1988947.png)

[//]: # (https://www.clipartmax.com/middle/m2i8b1d3G6Z5Z5H7_bush-maze-hedge/)  

Un labyrinthe rectangulaire délimité et entouré par des buissons peut être schématisé par un tableau 2D : une liste de listes d'entiers égaux à `0` ou `1`.

- `1` désigne un buisson dans lequel on s'entrave.
- `0` désigne un emplacement libre, où l'on peut circuler vers toute autre case libre immédiatement au Nord, au Sud, à l'Est ou à l'Ouest.
- Il y a au moins trois lignes et trois colonnes.
    - La première et la dernière ligne sont remplies de `1`.
    - La première et la dernière colonne sont remplies de `1`.
- Le départ est sur la deuxième ligne, deuxième colonne. Cette case est libre.
- La sortie est sur l'avant-dernière ligne, avant-dernière colonne.


Un chemin est décrit par une chaine de caractères composée de lettres parmi `"NSEO"`.

- Le Nord (`N`), c'est vers le haut du tableau ; $-1$ pour la ligne, même colonne.
- Le Sud (`S`), c'est vers le bas du tableau ; $+1$ pour la ligne, même colonne.
- L'Est (`E`), c'est vers la droite du tableau ; même ligne, $+1$ pour la colonne.
- L'Ouest (`O`), c'est vers la gauche du tableau ; même ligne, $-1$ pour la colonne.



Écrire une fonction telle que `verifie(labyrinthe, chemin)` détermine, en renvoyant un booléen, si la description `chemin` permet d'aller du départ à la sortie du `labyrinthe` sans passer deux fois sur la même case, ni passer par des buissons.

La fonction pourra modifier `labyrinthe` à loisir.

!!! example "Exemples"

    ```python
    labyrinthe  = [
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
        [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1],
        [1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1],
        [1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1],
        [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ]

    chemin_0 = "EEEEEO"
    chemin_1 = "EEEEES"
    chemin_2 = "SSSSSEENNNEEEEEEESSOOOOSSSEEEESS"

    
    labyrinthe_0 = [ligne.copy() for ligne in labyrinthe]
    labyrinthe_1 = [ligne.copy() for ligne in labyrinthe]
    labyrinthe_2 = [ligne.copy() for ligne in labyrinthe]

    assert not verifie(labyrinthe_0, chemin_0), "retour sur ses pas"
    assert not verifie(labyrinthe_1, chemin_1), "entrée dans les buissons"
    assert     verifie(labyrinthe_2, chemin_2)
    ```

    ![labyrinthe](labyrinthe.svg)

Code à compléter (constante `DECALAGE` et fonction `verifie`) :

{{ IDE('exo') }}
