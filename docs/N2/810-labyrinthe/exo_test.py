# tests

labyrinthe  = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1],
    [1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]

chemin_0 = "EEEEEO"
chemin_1 = "EEEEES"
chemin_2 = "SSSSSEENNNEEEEEEESSOOOOSSSEEEESS"

labyrinthe_0 = [ligne.copy() for ligne in labyrinthe]
labyrinthe_1 = [ligne.copy() for ligne in labyrinthe]
labyrinthe_2 = [ligne.copy() for ligne in labyrinthe]

assert not verifie(labyrinthe_0, chemin_0), "retour sur ses pas"
assert not verifie(labyrinthe_1, chemin_1), "entrée dans les buissons"
assert     verifie(labyrinthe_2, chemin_2)

# autres tests
import copy

# test minimaliste

lab2 = [
    [1, 1, 1],
    [1, 0, 1],
    [1, 1, 1],
]

lab = copy.deepcopy(lab2)
assert   verifie(lab, "")

lab = copy.deepcopy(lab2)
assert not verifie(lab, "N")

lab = copy.deepcopy(lab2)
assert not verifie(lab, "S")

lab = copy.deepcopy(lab2)
assert not verifie(lab, "E")

lab = copy.deepcopy(lab2)
assert not verifie(lab, "O")


# test 4×3

lab3 = [
    [1, 1, 1],
    [1, 0, 1],
    [1, 0, 1],
    [1, 1, 1],
]

lab = copy.deepcopy(lab3)
assert     verifie(lab, "S")

lab = copy.deepcopy(lab3)
assert not verifie(lab, "SNS")

lab = copy.deepcopy(lab3)
assert not verifie(lab, "N")
lab = copy.deepcopy(lab3)
assert not verifie(lab, "E")
lab = copy.deepcopy(lab3)
assert not verifie(lab, "O")


# test 3×4

lab3 = [
    [1, 1, 1, 1],
    [1, 0, 0, 1],
    [1, 1, 1, 1],
]

lab = copy.deepcopy(lab3)
assert     verifie(lab, "E")

lab = copy.deepcopy(lab3)
assert not verifie(lab, "EOE")

lab = copy.deepcopy(lab3)
assert not verifie(lab, "N")
lab = copy.deepcopy(lab3)
assert not verifie(lab, "S")
lab = copy.deepcopy(lab3)
assert not verifie(lab, "O")

