# tests

assert eratosthene(5) == [False, False, True, True, False, True]
assert eratosthene(6) == [False, False, True, True, False, True, False]
assert premiers_jusque(5) == [2, 3, 5]
assert premiers_jusque(6) == [2, 3, 5]
assert premiers_jusque(20) == [2, 3, 5, 7, 11, 13, 17, 19]


# autres tests

def est_premier(n):
    if n < 2: return False
    for d in range(2, n):
        if n%d == 0: return False
    return True


for n in range(37):
    attendu = [est_premier(p) for p in range(n + 1)]
    assert eratosthene(n) == attendu, f"Erreur avec n = {n}"
for n in range(100):
    attendu = [p for p in range(n + 1) if est_premier(p)]
    assert premiers_jusque(n) == attendu, f"Erreur avec n = {n}"
