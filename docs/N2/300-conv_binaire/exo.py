def conversion_binaire(n):
    restes = [n % 2]
    n = n // 2
    while ...:
        restes.append(...)
        n = ...
    # renverser la liste
    nb_bits = len(...)
    return [restes[...] for i in range(nb_bits)]




# tests

assert conversion_binaire(13) == [1, 1, 0, 1]
assert conversion_binaire(4) == [1, 0, 0]
assert conversion_binaire(0) == [0]
