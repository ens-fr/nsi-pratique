## Commentaires

{{ IDE('exo_corr') }}

Les trois premières méthodes (`__init__`, `donne_nom` et `donne_poids`) ne présentent pas de difficultés. On notera cependant l'utilisation de l'argument `self` qui permet d'indiquer que cette méthode s'applique à l'objet concerné par l'appel. Lors de l'utilisation de ces méthodes on omet ce paramètre : `chien.donne_nom()` par exemple, en effet il est passé avec `chien`.

Les trois autres méthodes (`machouille`, `aboie` et `mange`) prennent deux arguments : `self` (utilisé préfixé) et un paramètre à fournir postfixé lors de l'appel.

Dans le cas de la méthode `mange`, on vérifie que `ration` satisfait les conditions à l'aide d'un test `if 0 < ration <= self.poids / 10:`. Il serait aussi possible d'utiliser un opérateur `and` : `if 0 < ration and ration <= self.poids / 10:`.

On pourrait enfin utiliser une assertion `assert 0 < ration <= self.poids / 10:`. Toutefois, cette façon de procéder génèrerait une erreur si la condition n'est pas respectée. L'utilisation du test classique permet de gérer précisément les deux cas de figure et de renvoyer `False` en cas d'échec du test.
