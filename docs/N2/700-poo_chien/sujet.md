---
author: Nicolas Revéret
title: Chien en POO
tags:
  - 7-POO
---

# La classe Chien

On souhaite dans cet exercice créer une classe `Chien` ayant deux attributs :

* un nom `nom` de type `str`,

* un poids `poids` de type `float`.

Cette classe possède aussi différentes méthodes décrites ci-dessous (`chien` est un objet de type `Chien`) :

* `chien.donne_nom()` qui renvoie la valeur de l'attribut `nom` ;

* `chien.donne_poids()` qui renvoie la valeur de l'attribut `poids` ;

* `chien.machouille(jouet)` qui renvoie son argument, la chaine de caractères `jouet`, privé de son dernier caractère ;

* `chien.aboie(nb_fois)` qui renvoie la chaine `'Ouaf' * nb_fois`, où `nb_fois` est un entier passé en argument ;

* `chien.mange(ration)` qui modifie l'attribut `poids` en lui ajoutant la valeur de l'argument `ration` (de type `float`).


```mermaid
classDiagram
class Chien{
       nom : str
       poids : float
       donne_nom() str
       donne_poids() float
       machouille(jouet : str) str
       aboie(nombre : int) str
       mange(ration : float) bool
}
```

On ajoute les contraintes suivantes concernant la méthode `mange` :

* on vérifiera que la valeur de `ration` est comprise entre 0 (exclu) et un dixième du poids du chien (inclus),
* la méthode renverra `True` si `ration` satisfait ces conditions et que l'attribut `poids` est bien modifié, `False` dans le cas contraire.

!!! example "Exemples"

    ```pycon
    >>> medor = Chien('Médor', 12.0)
    >>> medor.donne_nom()
    'Médor'
    >>> medor.donne_poids()
    12.0
    >>> medor.machouille('bâton')
    'bâto'
    >>> medor.aboie(3)
    'OuafOuafOuaf'
    >>> medor.mange(2.0)
    False
    >>> medor.mange(1.0)
    True
    >>> medor.donne_poids()
    13.0
    >>> medor.mange(1.3)
    True
    ```

Compléter le code de la classe `Chien`.

{{ IDE('exo') }}

??? tip "Aide pour `machouille`"

    Pour la méthode `machouille` on pourra recopier toutes les lettres de `jouet` sauf la dernière en parcourant les indices à l'aide
    de la boucle `for i in range(len(jouet) - 1):`
