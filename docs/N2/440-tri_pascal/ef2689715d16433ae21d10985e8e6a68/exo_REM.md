## Commentaires

{{ IDE('exo_corr') }}

On travaille par accumulation de lignes à partir de la liste `[[1]]` qui contient la ligne 0 qui ne contient qu'un élément.

On fait faire $n$ tours de boucles pour ajouter les lignes $1$ incluse à $n+1$ exclue.

Pour chaque ligne, le premier, comme le dernier terme sont ajoutés individuellement ; il s'agit de $1$.

Les autres termes sont calculés en suivant la formule donnée. Il faut faire attention aux indices.

- On calcule la ligne $k$, on utilise donc les coefficients de la ligne $k-1$. Ceux aux indices $i-1$ et $i$ ; au premier tour de boucle interne $i=1$, et on ajoute bien les termes d'indice 0 et 1.


### Utilisations du triangle de Pascal

Le triangle de Pascal donne les coefficients binomiaux qui sont très utilisés en combinatoire.

La formule du binôme est

$$(a+b)^n = \sum_{i=0}^{n}\binom{n}{i}a^{n-i}b^i$$

La ligne d'indice 3 du triangle est `[1, 3, 3, 1]`, ce qui donne par exemple

$$(a+b)^3 = a^3 + 3a^2b + 3ab^2 + b^3$$

### Histoire

En mathématiques, le triangle de Pascal est une présentation des coefficients binomiaux dans un triangle. Il fut nommé ainsi en l'honneur du mathématicien français Blaise Pascal. Il est connu sous l'appellation « triangle de Pascal » en Occident, bien qu'il ait été étudié par d'autres mathématiciens, parfois plusieurs siècles avant lui, en Inde, en Perse (où il est appelé « triangle de Khayyam »), au Maghreb, en Chine (où il est appelé « triangle de Yang Hui »), en Allemagne et en Italie (où il est appelé « triangle de Tartaglia »).

Source : [Wikipédia, Le triangle de Pascal](https://fr.wikipedia.org/wiki/Triangle_de_Pascal)