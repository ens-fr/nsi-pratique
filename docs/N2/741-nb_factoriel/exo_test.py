# tests

assert factorielle(5) == 120
assert factorielle(10) == 3628800

# autres tests

def f(n):
    if n == 0:
        return 1
    else:
        return n * f(n - 1)

for n in [0, 1, 2, 3, 4, 17, 42, 11, 42, 31]:
    attendu = f(n)
    assert factorielle(n) == attendu, f"Erreur avec {n=}"
