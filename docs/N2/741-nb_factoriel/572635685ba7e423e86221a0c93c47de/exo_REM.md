## Commentaires

### Réponse

{{ IDE('exo_corr') }}

Au début du code, `i` est le premier indice absent de `factorielle_mem`. Ainsi le prochain nombre à ajouter est $i!$.

Cette propriété est conservée à la fin de la boucle `while` ; on parle alors d'**invariant** de boucle.

En sortie de boucle, on a `n < i` or `i` est le premier indice absent, ce qui prouve que l'indice `n` est présent. On peut alors répondre.


### Version raccourcie

Parfois une version raccourcie peut être plus explicite.

```py
factorielle_mem = [1]

def factorielle(n):
    i = len(factorielle_mem)
    while n >= i:
        factorielle_mem.append(factorielle_mem[i - 1] * i)
        i += 1
    return factorielle_mem[n]
```

