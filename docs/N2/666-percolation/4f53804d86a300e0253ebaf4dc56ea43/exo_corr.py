TERRE = 1
VIDE = 0
EAU = 2


def percolation(sol, i, j, prof_max):
    sol[i][j] = EAU

    if i == prof_max:
        return True
    else:
        if sol[i+1][j] == VIDE:
            if percolation(sol, i+1, j, prof_max):
                return True

        if sol[i][j-1] == VIDE:
            if percolation(sol, i, j-1, prof_max):
                return True

        if sol[i][j+1] == VIDE:
            if percolation(sol, i, j+1, prof_max):
                return True

        return False


def nouveau_sol(nom='base'):
    """
    Renvoie un sol sec
    Trois sols sont proposés
    Par défaut on récupère le sol 'base'
    """
    sols = {'base': [
        [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
        [1, 1, 0, 1, 0, 1, 1, 1, 0, 1],
        [1, 1, 1, 1, 0, 1, 1, 0, 0, 1],
        [1, 0, 1, 0, 1, 0, 0, 0, 1, 1],
        [1, 0, 1, 1, 1, 1, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ],
        'puits': [
        [1, 0, 1],
        [1, 0, 1],
        [1, 0, 1],
        [1, 0, 1],
        [1, 1, 1],
    ],
        'diagonale': [
        [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 0, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ]

    }
    return [ligne.copy() for ligne in sols[nom]]


# Tests
# Test profondeur 0 (déjà atteinte)
sol = nouveau_sol()
assert percolation(sol, 0, 5, 0)
# Test profondeur 1 (atteignable)
sol = nouveau_sol()
assert percolation(sol, 0, 5, 1)
# Test profondeur 5 (atteignable)
sol = nouveau_sol()
assert percolation(sol, 0, 5, 5)
# Test profondeur 6 (non atteignable)
sol = nouveau_sol()
assert not percolation(sol, 0, 5, 6)
