# Exercices pratiques de NSI


## Présentation 👩‍💻 🧑‍💻

Ce site propose des exercices d'entrainement à la pratique de la programmation en lien avec la spécialité NSI. Le langage utilisé est Python.[^python]

[^python]: Le langage Python : [wiki officiel français](https://wiki.python.org/moin/FrenchLanguage).

- Les exercices proposés sont créés, discutés, testés par un collectif de professeurs d'informatique, de lycée ou du supérieur.
- Aucune installation, ni inscription n'est nécessaire ; tous les programmes sont exécutés sur votre machine, tablette ou téléphone.
- Pour garder une trace de votre travail, il est possible de le sauvegarder sur votre machine ; c'est facultatif.


!!! info "Sobriété numérique"
    Ce site comprend une base importante d'exercices, avec une charte graphique que l'on espère agréable pour tous. Pour autant, le site reste très léger, utilisable même sur téléphone ou tablette. Les images sont le plus souvent vectorielles, présentes uniquement dans certains exercices.


## Fonctionnement ⚙️

Pour chaque exercice, on trouve un énoncé, souvent pour créer une fonction, puis des exemples d'application de la fonction. Ensuite, une zone de saisie du code qui est accompagné des premiers tests. Il y a des tests secrets qui permettent ensuite d'accepter (ou valider) votre code.

Sous cette dernière zone vous trouverez quatre boutons :

| Lancer | Télécharger | Téléverser | Vérifier |
|:------:|:-----------:|:----------:|:--------:|
|![Exécuter](images/buttons/icons8-play-64.png){ .md-button .md-button--primary }|![Télécharger](images/buttons/icons8-download-64.png){ .md-button .md-button--primary }|![Téléverser](images/buttons/icons8-upload-64.png){ .md-button .md-button--primary }|![Vérifier](images/buttons/icons8-check-64.png){ .md-button .md-button--primary }|



1. On **lance** l'exécution du code avec ses premiers tests. Cela permet de visualiser les éventuelles premières erreurs.
2. Pour travailler dans votre IDE [^editors] préféré ou sauvegarder votre travail, on peut **télécharger** le code et l'enregistrer dans un fichier.
3. On peut **téléverser** un code sauvegardé depuis votre appareil. Votre code fonctionne en local... mais passe-t-il nos _tests secrets_ ?
4. On lance le l'exécution du code, mais aussi _des tests secrets_. Pour **vérifier** si on a réussi l'exercice !

[^editors]: Les éditeurs recommandés par l'équipe sont :
  
    - [Thonny](https://thonny.org/) pour les débutants
    - [VSCodium](https://vscodium.com/) pour les utilisateurs avancés

## Où commencer 🧭

Vous pouvez choisir les exercices de plusieurs façons :

* en choisissant une catégorie avec le menu de navigation.

* en choisissant de travailler un thème particulier. Les exercices sont *taggés* et regroupés à la page suivante `🏷️ Tags`

## Construction du site 🛠

Ce site est rédigé en Markdown, construit de façon statique avec MkDocs et le thème Material. [^material]

[^material]: [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) repose sur
  
    - le langage [Markdown](https://markdown.org)
    - le générateur de site statique [MkDocs](https://mkdocs.org)

Les logiciels qui servent à construire et publier ce site sont des logiciels libres [^ll] et respectent les 4 libertés :

1. Utiliser le logiciel pour tous les usages
2. Étudier le code source du logiciel
3. Distribuer le logiciel de façon libre
4. Améliorer le logiciel et distribuer les améliorations.

[^ll]: April : [Introduction aux logiciels libres](https://www.april.org/articles/divers/intro_ll.html)


!!! info "RGPD"
    Le site fonctionne donc sans _tracker_ de suivi ni _cookies_ ; aucune mesure d'audience. Le code JavaScript exécuté sert pour la console de développement intégré sur les pages et nous l'hébergeons également. Seul MathJax [^math] est servi depuis un réseau de diffusion de contenu (ou _CDN_).

    [^math]: [MathJax](https://www.mathjax.org/) est utilisé pour le rendu des formules mathématiques, il est servi par [jsdeliver.net](https://www.jsdelivr.com/) sur ce site.

Les auteurs des exercices sont cités dans le code source `html` de la page, ainsi que dans le code source du site.

Si vous souhaitez réaliser un site avec une technologie similaire dans le cadre de votre enseignement de la NSI, voici deux excellents points de départ : 

- <https://ens-fr.gitlab.io/tuto-markdown/> puis <https://ens-fr.gitlab.io/mkdocs/>
- <https://bouillotvincent.gitlab.io/pyodide-mkdocs/>
