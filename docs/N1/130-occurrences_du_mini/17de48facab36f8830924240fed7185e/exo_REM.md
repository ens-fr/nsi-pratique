## Commentaires

### Version simple

{{ IDE('exo_corr') }}

### Version fonctionnelle

On pourrait écrire un code avec un style plus fonctionnel

```python
def occurrences_mini(donnees):
    mini = donnees[0]
    indices = []
    for i, valeur in enumerate(donnees):
        if valeur == mini:
            indices.append(i)
        elif valeur < mini:
            mini = valeur
            indices = [i]
    return (mini, indices)
```

`enumerate` est utile lorsqu'on a besoin de l'indice et de la valeur.

### Version décalée

```python
def occurrences_mini(donnees):
    mini = donnees[0]
    indices = [0]
    for i in range(1, len(donnees)):
        valeur = donnees[i]
        if valeur == mini:
            indices.append(i)
        elif valeur < mini:
            mini = valeur
            indices = [i]
    return (mini, indices)
```

On initialise `indices` avec le premier élément, on peut alors commencer à l'indice 1, mais la traduction en fonctionnel est moins évidente... voire délicate.
