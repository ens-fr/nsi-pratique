# Commentaires

{{ IDE('exo_corr') }}

Il n'y a pas de difficultés majeures si ce n'est prendre soin de ne pas modifier la liste initiale.

On crée donc une seconde liste que l'on complète au fur et à mesure en parcourant les valeurs de `valeurs`.

On aurait aussi pu utiliser une liste par compréhension et un *opérateur ternaire* :

```python
def remplacer(valeurs, valeur_cible, nouvelle_valeur):
    return [x if x != valeur_cible else nouvelle_valeur for x in valeurs]
```