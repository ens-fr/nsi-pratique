# Une solution possible

{{ IDE('exo_corr') }}

On débute par la création d'un dictionnaire vide `emails_pseudos` qui contiendra les informations souhaitées.

On parcourt ensuite l'ensemble des clés du dictionnaire `emails_ids`. Par construction ce sont les adresses email de tous les abonnés.

Dans chaque cas, on vérifie que l'id correspondante est une clé du second dictionnaire : `emails_ids[email]` in `ids_pseudos`. Si oui, on ajoute le pseudo correspondant dans le dictionnaire `emails_pseudos`. Si non, on ajoute l'adresse email comme valeur.

## Autre solution

La méthode `items` des dictionnaires permet de parcourir les couples `(clé, valeur)` d'un dictionnaire. On peut l'utiliser afin de simplifier le parcours :

```python
def jointure(emails_ids, ids_pseudos):
    emails_pseudos = {}
    for email, identifiant in emails_ids.items():
        if identifiant in ids_pseudos:
            emails_pseudos[email] = ids_pseudos[identifiant]
        else:
            emails_pseudos[email] = email
    return emails_pseudos
```