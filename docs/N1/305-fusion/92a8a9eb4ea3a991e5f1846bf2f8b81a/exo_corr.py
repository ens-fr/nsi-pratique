def jointure(emails_ids, ids_pseudos):
    emails_pseudos = {}
    for email in emails_ids:
        identifiant = emails_ids[email]
        if identifiant in ids_pseudos:
            emails_pseudos[email] = ids_pseudos[identifiant]
        else:
            emails_pseudos[email] = email
    return emails_pseudos

# Tests
emails_ids = {"alice@fake.com": 1, "bob@bidon.fr": 2, "chris@false.uk": 3}
ids_pseudos = {1: "alice", 2: "B0b", 3: "ChristoF"}
assert jointure(emails_ids, ids_pseudos) == {
    "alice@fake.com": "alice",
    "bob@bidon.fr": "B0b",
    "chris@false.uk": "ChristoF",
}

emails_ids = {"alice@fake.com": 1, "bob@bidon.fr": 2, "chris@false.uk": 3}
ids_pseudos = {1: "alice", 2: "B0b"}
assert jointure(emails_ids, ids_pseudos) == {
    "alice@fake.com": "alice",
    "bob@bidon.fr": "B0b",
    "chris@false.uk": "chris@false.uk",
}

emails_ids = {"alice@fake.com": 5, "bob@bidon.fr": 6, "chris@false.uk": 3}
ids_pseudos = {5: "Alic3", 3: "ChristoF"}
assert jointure(emails_ids, ids_pseudos) == {
    "alice@fake.com": "Alic3",
    "bob@bidon.fr": "bob@bidon.fr",
    "chris@false.uk": "ChristoF",
}
