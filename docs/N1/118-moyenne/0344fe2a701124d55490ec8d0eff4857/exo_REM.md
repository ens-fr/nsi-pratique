## Commentaires

{{ IDE('exo_corr') }}

On utilise une fonction `sont_proches` pour tester la presque égalité entre flottants.

On ne devrait jamais faire de test d'égalité entre flottant !
