---
author: Sébastien HOARAU
title: Moyenne simple 
tags:
    - 1-boucle
---

Écrire une fonction `moyenne` prenant en paramètre un tableau non vide d'entiers et qui renvoie la moyenne des valeurs du tableau.

> Dans cet exercice, on n'utilisera pas la fonction prédéfinie `sum` ni aucune autre fonction de quelque module que se soit calculant la moyenne.

!!! example "Exemples"

    ```pycon
    >>> moyenne([10, 20, 30, 40, 60, 110])
    45.0
    >>> moyenne([1, 3])
    2.0
    >>> moyenne([44, 51, 12, 72, 65, 34])
    46.333333333333336
    ```

{{ IDE('exo', SANS="sum") }}
