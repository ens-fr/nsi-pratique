# 🗒️ Présentation

On trouve dans cette catégorie des exercices classiques, plutôt faciles.

Il faudra bien lire l'énoncé, et peut-être aussi prendre du papier et un crayon. Les exemples sont là pour aider à comprendre la consigne ; il faut y porter une grande attention.


![](./KnuthAtOpenContentAlliance.jpg)

> [Donald Knuth](https://fr.wikipedia.org/wiki/Donald_Knuth), né le 10 janvier 1938 à Milwaukee au Wisconsin, est un informaticien et mathématicien américain de renom, professeur émérite en informatique à l'université Stanford (en tant que « professeur émérite de l'art de programmer »). Il est un des pionniers de l'algorithmique et a fait de nombreuses contributions dans plusieurs branches de l'informatique théorique.
