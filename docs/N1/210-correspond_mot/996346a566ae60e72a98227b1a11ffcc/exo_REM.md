## Commentaires

### Version simple

{{ IDE('exo_corr') }}


### Version fonctionnelle

```python
def correspond(mot_complet, mot_a_trous):
    return len(mot_complet) == len(mot_a_trous) and all(
            mot_a_trous[i] == mot_complet[i] or mot_a_trous[i] == '.'
                for i in range(len(mot_complet))
        )
```

La fonction `all` fait aussi une évaluation paresseuse et s'interrompt dès que `False` est rencontré.
