# tests

assert correspond('INFORMATIQUE', 'INFO.MA.IQUE') == True
assert correspond('AUTOMATIQUE', 'INFO.MA.IQUE') == False
assert correspond('INFO', 'INFO.MA.IQUE') == False
assert correspond('INFORMATIQUES', 'INFO.MA.IQUE') == False

# autres tests

assert correspond('', '') == True
assert correspond('A', 'A') == True
assert correspond('A', '.') == True
assert correspond('A', 'B') == False
assert correspond('TIQUE', 'TIQUE') == True
assert correspond('TIQUE', '.IQUE') == True
assert correspond('TIQUE', 'T.QUE') == True
assert correspond('TIQUE', 'TI.UE') == True
assert correspond('TIQUE', 'TIQ.E') == True
assert correspond('TIQUE', 'TIQU.') == True
assert correspond('TIQUE', '.IQU.') == True
assert correspond('TIQUE', '.....') == True
assert correspond('TIQUE', 'TIQUES') == False
assert correspond('TIQUE', 'ATIQUE') == False
assert correspond('TIQUE', '.TIQUE') == False
assert correspond('TIQUE', 'TIQUE.') == False
assert correspond('TIQUE', 'TIQUF') == False
assert correspond('TIQUE', 'PIQUE') == False

assert correspond("AB", "..") == True
assert correspond("BCA", ".B.") == False
assert correspond("A", "..") == False
assert correspond("A" * 101, "A" * 100 + ".") == True
