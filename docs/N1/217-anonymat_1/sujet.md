---
author: Nicolas Revéret
title: Anonymat (1)
tags:
    - 2-string
status: relecture
---

# Noircir un texte

Avant de rendre public des dossiers sensibles, il arrive que certains organismes les « anonyment » en entier ou partiellement.

Dans le cadre de cet exercice, nous ne programmons qu'une version simpliste : « noircir » un texte consiste à **remplacer toutes les caractères alphabétiques par un caractère occultant**, comme par exemple `▮`.

Le texte `L'espion était J. Bond` devient alors `▮'▮▮▮▮▮▮ ▮▮▮▮▮ ▮. ▮▮▮▮`.

On demande d'écrire la fonction `noircir` qui prend en argument une chaîne de caractères `texte` ainsi que le caractère occultant `noir` et renvoie le même texte « noirci » du début à la fin.

!!! tip "Astuce"

    Si `s` est une chaîne de caractères, l'instruction `s.isalpha()` renvoie `True` si `s` n'est composée que de caractères alphabétiques.
    
    Ainsi `"é".isalpha()` est évalué à `True`, `"Bonjour !".isalpha()` est évalué à `False`.

!!! example "Exemples"

    ```pycon
    >>> noircir("", "*")
    ''
    >>> noircir("L'espion était J. Bond", "▮")
    "▮'▮▮▮▮▮▮ ▮▮▮▮▮ ▮. ▮▮▮▮"
    >>> noircir("L'espion était J. Bond", "_")
    "_'______ _____ _. ____"
    >>> noircir(";-)", "▮")
    ';-)'
    ```

    
{{ IDE('exo') }}
