def inversions(tableau):
    ...


# Tests
assert inversions([]) == 0
assert inversions([5, 6, 7, 9]) == 0
assert inversions([7, 5, 9, 6]) == 3