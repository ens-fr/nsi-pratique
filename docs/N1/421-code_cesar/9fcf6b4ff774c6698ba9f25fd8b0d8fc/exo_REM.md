## Commentaires

{{ IDE('exo_corr') }}

### Aspect sécurité

Jules César utilisait cette méthode avec l'alphabet grec, non compris de la plupart des gaulois, mais maitrisé par l'élite romaine.

Cette méthode de chiffrement n'est pas sécurisée et très facile
à casser en tentant tous les décalages possibles ou en faisant une
analyse des fréquences des lettres.

Aujourd'hui, elle est utilisée dans les livres-jeu où on souhaite donner un indice masqué, facilement accessible.
