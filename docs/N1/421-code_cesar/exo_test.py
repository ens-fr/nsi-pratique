# tests
assert cesar('HELLO WORLD!', 5) == 'MJQQT BTWQI!'
assert cesar('MJQQT BTWQI!', -5) == 'HELLO WORLD!'
assert cesar('BONJOUR LE MONDE !', 23) == 'YLKGLRO IB JLKAB !'
assert cesar('YLKGLRO IB JLKAB !', -23) == 'BONJOUR LE MONDE !'


# autres tests

assert cesar('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 1) == 'BCDEFGHIJKLMNOPQRSTUVWXYZA'
assert cesar('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 24) == 'YZABCDEFGHIJKLMNOPQRSTUVWX'
assert cesar('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 0) == 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
assert cesar('', 7) == ''
assert cesar('!?.:', 7) == '!?.:'
assert cesar('CESAR', 139) == 'LNBJA'
