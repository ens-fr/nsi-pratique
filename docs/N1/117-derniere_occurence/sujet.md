---
author: Sébastien HOARAU
title: Dernière occurrence 
tags:
    - 1-boucle
---

Programmer la fonction `derniere_occurrence`, prenant en paramètre un tableau non vide d'entiers et un entier `cible`, et qui renvoie l'indice de la **dernière** occurrence de `cible`.

Si l'élément n'est pas présent, la fonction renvoie la longueur du tableau.

> On n'utilisera pas la fonction `index`

!!! example "Exemples"

    ```python
    >>> derniere_occurrence([5, 3], 1)
    2
    ```
    `1` est absent, on renvoie la longueur du tableau : `2`.

    ```pycon
    >>> derniere_occurrence([2, 4], 2)
    0
    ```
    `2` est présent à l'indice `0`, on renvoie `0`.

    ```pycon
    >>> derniere_occurrence([2, 3, 5, 2, 4], 2)
    3
    ```
    `2` est présent aux indices `0` et `3`, on renvoie le dernier : `3`

{{ IDE('exo', SANS="index") }}
