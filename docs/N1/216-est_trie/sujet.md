---
author: Sébastien HOARAU
title: Est trié ? 
tags:
    - 1-boucle
    - 2-booléen
---

Programmer la fonction `est_trie` qui prend en paramètre un tableau de valeurs numériques et qui détermine si ce tableau est trié dans l'ordre croissant. La fonction renvoie un booléen `True` ou `False`.

> On n'utilisera ni `sort` ni `sorted` dans cet exercice.

!!! example "Exemples"

    ```pycon
    >>> est_trie([0, 5, 8, 8, 9])
    True
    >>> est_trie([8, 12, 4])
    False
    >>> est_trie([-1, 4])
    True
    >>> est_trie([5])
    True
    >>> est_trie([])
    True
    ```

{{ IDE('exo', SANS="sort, sorted") }}
