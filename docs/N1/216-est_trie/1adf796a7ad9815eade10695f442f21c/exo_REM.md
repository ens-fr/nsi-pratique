## Commentaires

### Version simple

{{ IDE('exo_corr') }}

Notons que dans les cas où `tableau` à strictement moins de 2 éléments `#!py range(1, len(tableau)` ne fait aucun tour de boucle.

### Version fonctionnelle

On peut coder cette phrase logique :

> Le tableau est trié s'il a moins de 2 éléments ou alors si chacun des éléments à partir du 2e est supérieur ou égal à son prédécesseur.

```python
def est_trie(tableau):
    return all(tableau[i] >= tableau[i - 1] for i in range(1, len(tableau)))
```

Notons que dans les cas où `tableau` à strictement moins de 2 éléments `#!py range(1, len(tableau)` ne fait aucun tour et `#!py all(...)` renvoie `True`.

