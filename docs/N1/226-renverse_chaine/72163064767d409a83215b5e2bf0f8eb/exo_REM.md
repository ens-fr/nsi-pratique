## Commentaires

{{ IDE('exo_corr') }}

On crée tout d'abord une chaine vide destinée à contenir la chaine `mot` renversée. Ensuite, on ajoute chaque caractère de `mot` **au début** de `tom`.

## Variante avec les indices

On pouvait aussi parcourir les indices de la fin vers le début.

```python
def renverser(mot):
    tom = ''
    for i in range(len(mot)-1, -1, -1):
        tom = tom + mot[i]
    return tom
```

## Variante fonctionnelle

Avec une liste par compréhension et on utilise `join` :

```python
def renverser(mot):
    return ''.join([mot[i] for i in range(len(mot)-1, -1, -1)])
```
