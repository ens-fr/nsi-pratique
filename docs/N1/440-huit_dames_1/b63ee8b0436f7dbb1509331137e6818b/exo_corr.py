def donne_ligne(echiquier, i):
    return echiquier[i]


def donne_colonne(echiquier, j):
    return [ligne[j] for ligne in echiquier]


def donne_diagonale_NE(echiquier, i, j):
    n = len(echiquier)
    resultat = []
    while i >= 0 and j < n:
        resultat.append(echiquier[i][j])
        i -= 1
        j += 1
    return resultat


def donne_diagonale_NO(echiquier, i, j):
    resultat = []
    while i >= 0 and j >= 0:
        resultat.append(echiquier[i][j])
        i -= 1
        j -= 1
    return resultat


def donne_diagonale_SE(echiquier, i, j):
    n = len(echiquier)
    resultat = []
    while i < n and j < n:
        resultat.append(echiquier[i][j])
        i += 1
        j += 1
    return resultat


def donne_diagonale_SO(echiquier, i, j):
    n = len(echiquier)
    resultat = []
    while i < n and j >= 0:
        resultat.append(echiquier[i][j])
        i += 1
        j -= 1
    return resultat


def disposition_valide(echiquier):
    n = len(echiquier)
    # Les lignes et les colonnes
    for indice in range(n):
        if sum(donne_ligne(echiquier, indice)) > 1:
            return False
        if sum(donne_colonne(echiquier, indice)) > 1:
            return False

    # Les diagonales
    for indice in range(n - 1):
        if sum(donne_diagonale_NE(echiquier, n - 1, indice)) > 1:
            return False
        if sum(donne_diagonale_SE(echiquier, indice, 0)) > 1:
            return False
    for indice in range(1, n - 1):
        if sum(donne_diagonale_NE(echiquier, indice, 0)) > 1:
            return False
        if sum(donne_diagonale_SE(echiquier, 0, indice)) > 1:
            return False

    return True


# Tests
valide = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

invalide = [[0, 0, 1], [1, 0, 0], [0, 1, 0]]

assert donne_ligne(valide, 0) == [0, 0, 0, 1, 0, 0, 0, 0]
assert donne_colonne(valide, 2) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_NE(valide, 4, 0) == [0, 0, 1, 0, 0]
assert donne_diagonale_NO(valide, 1, 1) == [0, 0]
assert donne_diagonale_SE(valide, 0, 0) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_SO(valide, 3, 7) == [1, 0, 0, 0, 0]
assert disposition_valide(valide)
assert not disposition_valide(invalide)
