## Commentaires

### Solution

{{ IDE('exo_corr') }}

Le problème des huit dames est un classique de la programmation. La vérification que la disposition est valide n'est que la surface du problème : la vraie difficulté est de générer l'ensemble des dispositions valides !

Les quatre fonctions `donne_diagonale` permettent de choisir le type de vérifications que l'on souhaite effectuer. On choisit dans le corrigé de tester :

* les diagonales orientées au nord-est débutant sur une case du bord gauche (sauf sur la première ligne car la "diagonale" ne comprend alors qu'une seule case) ou du bord inférieur (sauf sur la dernière colonne pour la même raison),
* les diagonales orientées au sud-est débutant sur une case du bord gauche (sauf sur la dernière ligne) ou du bord supérieur (sauf sur la dernière colonne).

Dans la fonction `disposition_valide`, on vérifie successivement que chaque ligne, colonne et diagonale contient au maximum une dame. Si ce n'est pas le cas, on renvoie
immédiatement `False`. Si tous les tests sont passés avec succès, la grille est valide : on renvoie `True`.

### Version fonctionnelle

Il est possible d'écrire une version plus fonctionnelle de `disposition_valide` en utilisant la fonction `all` qui renvoie `True` si tous les booléens contenus dans la liste passée en argument sont égaux à `True` :

```python
def disposition_valide(echiquier):
    n = len(echiquier)
    return     all(sum(donne_ligne(echiquier, indice)) <= 1 for indice in range(n)) \
           and all(sum(donne_colonne(echiquier, indice)) <= 1 for indice in range(n)) \
           and all(sum(donne_diagonale_NE(echiquier, n - 1, indice)) <= 1 for indice in range(n - 1)) \
           and all(sum(donne_diagonale_SE(echiquier, indice, 0)) <= 1 for indice in range(n - 1)) \
           and all(sum(donne_diagonale_NE(echiquier, indice, 0)) <= 1 for indice in range(1, n - 1)) \
           and all(sum(donne_diagonale_SE(echiquier, 0, indice)) <= 1 for indice in range(1, n - 1))
```