def donne_ligne(echiquier, i):
    ...


def donne_colonne(echiquier, j):
    ...


def donne_diagonale_NE(echiquier, i, j):
    ...


def donne_diagonale_NO(echiquier, i, j):
    ...


def donne_diagonale_SE(echiquier, i, j):
    ...


def donne_diagonale_SO(echiquier, i, j):
    ...


def disposition_valide(echiquier):
    ...


# Tests
valide = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

invalide = [[0, 0, 1], [1, 0, 0], [0, 1, 0]]

assert donne_ligne(valide, 0) == [0, 0, 0, 1, 0, 0, 0, 0]
assert donne_colonne(valide, 2) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_NE(valide, 4, 0) == [0, 0, 1, 0, 0]
assert donne_diagonale_NO(valide, 1, 1) == [0, 0]
assert donne_diagonale_SE(valide, 0, 0) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_SO(valide, 3, 7) == [1, 0, 0, 0, 0]
assert disposition_valide(valide)
assert not disposition_valide(invalide)
