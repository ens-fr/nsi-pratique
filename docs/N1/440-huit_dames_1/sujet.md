---
author: Nicolas Revéret
title: Huit dames
tags:
    - 4-grille
    - 4-fonctions
---

# Problème des huit dames

Aux échecs, la dame est capable de se déplacer dans toutes les directions.

![Mouvement des dames](mvt_dame.svg){width="40%"}

Le problème des huit dames consiste à placer 8 dames sur un échiquier classique (8 lignes et 8 colonnes) de façon à ce qu'aucune d'entre elles ne soit menacée par une autre. Ainsi il n'y a qu'une seule dame par ligne, colonne ou diagonale. La figure ci-dessous illustre une disposition valide :

![Disposition valide](valide_1.svg){width="40%"}

On considère dans cet exercice des « échiquiers » de taille $n \ge 1$ variable. On garantit que l'échiquier est bien carré. Selon la taille de l'échiquier, on pourra placer plus ou moins de dames.

Ces échiquiers seront représentés en machine par des listes de listes contenant :

* `0` si la case correspondante est vide,
* `1` si elle contient une dame.

???+ example "Exemple"

    La liste ci-dessous représente la disposition valide donnée plus haut.

    ```python
    valide = [
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0]
    ]
    ```

    La liste ci-dessous représente une disposition dans un échiquier de taille 3 : 

    
    ```python
    invalide = [
        [0, 0, 1],
        [1, 0, 0],
        [0, 1, 0]
    ]
    ```

    Cette seconde disposition est invalide car deux dames sont sur une même diagonale (cases `invalide[1][0]` et `invalide[2][1]`).


Vous devez écrire différentes fonctions :

* `donne_ligne` : prend en argument une liste `echiquier` et l'indice d'une ligne `i` et renvoie la liste correspondant à cette ligne dans l'échiquier,

* `donne_colonne` : prend en argument une liste `echiquier` et l'indice d'une colonne `j` et renvoie la liste correspondant à cette colonne dans l'échiquier,

* `donne_diagonale_NE` : prend en argument une liste `echiquier` et les coordonnées d'une case (ligne `i` et colonne `j`) et renvoie la liste correspondant à la diagonale débutant dans la case indiquée et dirigée vers le haut à droite ↗ (au nord-est, "NE"),

* `donne_diagonale_NO` : identique à la précédente mais la diagonale est dirigée vers le haut à gauche ↖ (au nord-ouest, "NO"),

* `donne_diagonale_SE` : identique à la précédente mais la diagonale est dirigée vers le bas à droite ↘ (au sud-est, "SE"),

* `donne_diagonale_SO` : identique à la précédente mais la diagonale est dirigée vers le bas à gauche ↙ (au sud-ouest, "SO"),

* `disposition_valide` : prend en argument une liste `echiquier` et renvoie le booléen répondant à la question "les dames satisfont-elles le problème des huit dames ?".

??? tip "Aide (1)"
    
    Une fois les différentes lignes, colonnes et diagonales récupérées, on pourra vérifier qu'elles ne contiennent pas plus d'une dame en utilisant la fonction `sum` de Python :

    ```pycon
    >>> sum([0, 0, 0, 1, 0, 0, 1, 0])
    2
    ```

    On rappelle à ce titre que les dames sont signalées dans la grille par des valeurs `1` et les cases vides par des `0`.

??? tip "Aide (2)"

    On pourra (mais ce n'est pas une obligation) effectuer les vérifications illustrées par les figures suivantes :

    ![Lignes](recherche_lignes.svg){width="30%"} ![Colonnes](recherche_colonnes.svg){width="30%"}
    
    ![Diagonales NE](recherche_diagonales_NE.svg){width="30%"} ![Diagonale SE](recherche_diagonales_SE.svg){width="30%"}


!!! example "Exemples"

    On utilise les échiquiers `valide` et `invalide` définis plus haut.

    ```pycon
    >>> donne_ligne(valide, 0)
    [0, 0, 0, 1, 0, 0, 0, 0]
    >>> donne_colonne(valide, 2)
    [0, 0, 1, 0, 0, 0, 0, 0]
    >>> donne_diagonale_NE(valide, 4, 0)
    [0, 0, 1, 0, 0]
    >>> donne_diagonale_NO(valide, 1, 1)
    [0, 0]
    >>> donne_diagonale_SE(valide, 0, 0)
    [0, 0, 1, 0, 0, 0, 0, 0]
    >>> donne_diagonale_SO(valide, 3, 7)
    [1, 0, 0, 0, 0]
    >>> disposition_valide(valide)
    True
    >>> disposition_valide(invalide)
    False
    ```

{{ IDE('exo') }}
