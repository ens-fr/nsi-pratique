# tests
assert score_scrabble("KAYAK") == 32
assert score_scrabble("INFORMATIQUE") == 23
assert score_scrabble("") == 0

# autres tests
assert score_scrabble("K") == 10
assert score_scrabble("GIRAFE") == 10
assert score_scrabble("LAPSUS") == 8


