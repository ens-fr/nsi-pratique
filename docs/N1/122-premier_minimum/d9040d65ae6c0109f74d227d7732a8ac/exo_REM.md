## Commentaires

Il s'agit d'une recherche de minimum réalisée avec une approche gloutonne.

Si l'on cherche le minimum de l'ensemble de la liste, l'algorithme utilisé ici n'est pas optimal. Par contre, si l'on cherche comme ici le *premier minimum local*, l'algorithme donnera à coup sûr la bonne réponse.

{{ IDE('exo_corr') }}

La variable `i` contient l'indice de la dalle sur laquelle se trouve la balle. On l'initialise à `0`.

On parcourt ensuite les indices à partir de `0` tant que la hauteur de la dalle suivante est inférieure ou égale à celle de la dalle "actuelle".

La garantie que le dernier élément de la liste est le strictement supérieur aux précédents nous assure que l'on sortira bien de la boucle avant que `i` ne soit égal à `len(hauteurs)-1` (ce qui entrainerait une erreur au moment d'accéder à `hauteurs[i+1]`).

Une fois sorti de la boucle, on renvoie la valeur de l'indice `i`.
