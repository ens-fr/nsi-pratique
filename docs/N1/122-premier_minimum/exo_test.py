from random import randint

# Tests
hauteurs = [3, 2, 5]
assert indice_arret(hauteurs) == 1

hauteurs = [3, 5]
assert indice_arret(hauteurs) == 0

hauteurs = [10, 8, 7, 5, 5, 4, 3, 6, 6, 5, 4, 12]
assert indice_arret(hauteurs) == 6

# Tests supplémentaires
assert indice_arret([0, 1]) == 0
assert indice_arret([0]*10 + [1]) == 9
for _ in range(100):
    hauteurs = [randint(1, 100) for _ in range(randint(1, 100))] + [101]
    i = [k for k in range(len(hauteurs)-1) if hauteurs[k] < hauteurs[k+1]][0]
    assert indice_arret(hauteurs) == i
