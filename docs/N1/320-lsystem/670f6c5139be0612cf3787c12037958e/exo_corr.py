def transformation(motif, regles):
    resultat = ""
    for caractere in motif:
        if caractere in regles:
            resultat += regles[caractere]
        else:
            resultat += caractere
    return resultat


def n_transformations(motif, regles, n):
    for _ in range(n):
        motif = transformation(motif, regles)
    return motif
