---
author: Nicolas Revéret
title: Des règles et des lettres
tags: 
    - 2-string
    - 3-dictionnaire
---

# Des règles et des lettres

On se donne une chaine de caractères appelée `motif` ainsi qu'un ensemble de règles du type "*Si le caractère lu est un* `a` *, remplace-le par* `ab`".

L'ensemble de ces règles est stocké dans un dictionnaire Python dans lequel :

* une clé est un caractère lu,
* et la valeur associée est une chaine de caractères par laquelle le caractère doit être remplacé.

Si le caractère lu ne fait pas partie des clés du dictionnaire, il n'est associé à aucune règle et est donc recopié à l'identique.

!!! example "Exemple"

    On prend pour règles `#!py {'a': 'ab', 'b': 'ac', 'c': 'd'}`.

    On prend pour motif de départ `'a'`.

    Après **une** transformation on obtient `'ab'`.

Il est aussi possible d'effectuer cette transformation plusieurs fois de suite en l'appliquant à chaque étape au résultat de l'étape précédente.

!!! example "Exemple"

    On garde pour règles `#!py {'a': 'ab', 'b': 'ac', 'c': 'd'}`

    On prend pour encore motif de départ `'a'`

    Après **une** transformation on obtient `'ab'`

    Après **deux** transformations on obtient `'abac'`
    
    Après **trois** transformations on obtient `'abacabd'`


Vous devez écrire deux fonctions Python :

* `transformation(motif, regles)` prend en argument un motif initial (une chaine de caractères) ainsi qu'un ensemble de règles (un dictionnaire) et renvoie la chaine obtenue après **une** transformation.

* `n_transformations(motif, regles, n)` prend en argument un motif initial (une chaine de caractères), un ensemble de règles (un dictionnaire) ainsi qu'un entier `n` et renvoie le résultat obtenu après `n` transformations.

!!! example "Exemples"

    ```pycon
    >>> regles = {'a': 'ab', 'b': 'ac', 'c': 'd'}
    >>> motif = 'a'
    >>> transformation(motif, regles)
    'ab'
    >>> n_transformations(motif, regles, 2)
    'abac'
    >>> n_transformations(motif, regles, 3)
    'abacabd'
    >>> n_transformations("rien ne change !", {'z': 'y'}, 50)
    'rien ne change !'
    ```

{{ IDE('exo') }}
