## Commentaires

### Version itérative

{{ IDE('exo_corr') }}

On ajoute une espace avant d'ajouter un mot au discours, sauf si c'est le premier ; chose que l'on détecte quand le discours est vide.

### Version fonctionnelle

> L'énoncé interdit d'utiliser `join`

Voici ce que ça aurait pu donner : 1 ligne !

```python
def supprimheu(mots):
    return " ".join(mot for mot in mots if mot != "heu")
```

### Version avec filtre avant collage

Cette version n'est pas recommandée !

```python
def supprimeuh(mots):
    liste_mots = []
    for mot in mots:
        if mot != "heu":
            liste_mots.append(mot)
    if liste_mots == []:
        return ''
    chaine = liste_mots[0]
    for mot in liste_mots[1:]:
        chaine = chaine + ' ' + mot
    return chaine
```

### Autre version itérative

Dans la plupart des juges en ligne, la présence d'espaces supplémentaires en fin de ligne est acceptée. **Dans ce cas**, une solution serait :


```python
def supprimheu(mots):
    discours = ""
    for mot in mots:
        if mot != "heu":
            discours += mot + " "
    return discours
```

Dernière remarque : si on souhaite après coup enlever les espaces en trop à la fin, on peut utiliser `rstrip` ainsi : `#!py return discours.rstrip()`
