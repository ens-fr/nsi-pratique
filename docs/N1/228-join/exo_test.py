# tests

assert supprimheu(["je", "heu", "vais", "coder", "heu", "la",
 "fonction", "supprimheu"]) == 'je vais coder la fonction supprimheu'

assert supprimheu(["c", "est", "facile"]) == 'c est facile'


# autres tests

assert supprimheu([]) == ''
assert supprimheu(["heu"]) == ''
assert supprimheu(["heu", "bonjour"]) == 'bonjour'
assert supprimheu(["bien", "le", "bonjour"]) == 'bien le bonjour'
assert supprimheu(["bien", "le", "bonjour", "heu"]) == 'bien le bonjour'
