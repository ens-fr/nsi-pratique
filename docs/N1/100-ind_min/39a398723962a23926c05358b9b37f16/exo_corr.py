def indice_min(nombres):
    mini = nombres[0]
    i_mini = 0
    for i in range(len(nombres)):
        if nombres[i] < mini:
            mini = nombres[i]
            i_mini = i
    return i_mini


# tests

assert indice_min([5]) == 0
assert indice_min([2, 4, 1, 1]) == 2
assert indice_min([5, 3, 2, 5, 2]) == 2
