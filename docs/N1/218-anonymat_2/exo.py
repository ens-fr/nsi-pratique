def caviarder(texte, debut, fin):
    ...


# Tests
assert caviarder("L'espion était J. Bond", 15, 21) == "L'espion était #. ####"
assert caviarder("Paul est un espion", 100, 200) == "Paul est un espion"
