def denivele_positif(altitudes):
    cumul = 0
    for i in range(len(altitudes) - 1):
        if altitudes[i + 1] > altitudes[i]:
            cumul += altitudes[i + 1] - altitudes[i]
    return cumul


# tests

assert denivele_positif([330, 490, 380, 610, 780, 550]) == 560
assert denivele_positif([200, 300, 100]) == 100
