# tests

assert denivele_positif([330, 490, 380, 610, 780, 550]) == 560
assert denivele_positif([200, 300, 100]) == 100

# autres tests

assert denivele_positif([200]) == 0
assert denivele_positif([0, 1]) == 1
assert denivele_positif([1, 0]) == 0
assert denivele_positif([1, 0, 1]) == 1
assert denivele_positif([1, 2, 1]) == 1
assert denivele_positif([1, 1, 1]) == 0
assert denivele_positif([1, 2, 3]) == 2
assert denivele_positif([3, 2, 1]) == 0
