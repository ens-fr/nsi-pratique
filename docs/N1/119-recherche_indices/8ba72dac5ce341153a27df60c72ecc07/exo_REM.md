## Version recommandée

{{ IDE('exo_corr') }}

## Version sans compréhension de liste

```python
def indices(element, entiers):
    positions = []
    for i in range(len(entiers)):
        if entiers[i] == element:
            positions.append(i)
    return positions
```

## Version fonctionnelle

```python
def indices(element, entiers):
    return [i for i, a in enumerate(entiers) if a == element]
```
