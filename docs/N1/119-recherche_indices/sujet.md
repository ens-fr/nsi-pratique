---
author: Sébastien HOARAU
title: Recherche d'indices
tags:
    - 1-boucle
---

# Recherche des positions d'un élément dans un tableau

Écrire une fonction `indices` qui prend en paramètres un entier `element` et un tableau d'`entiers` et qui renvoie la liste croissante des indices de `element` dans le tableau `entiers`.

Cette liste sera donc vide `[]` si `element` n'apparait pas dans `entiers`.

> On n'utilisera ni la méthode `index`, ni la méthode `max`.

!!! example "Exemples"

    ```pycon
    >>> indices(3, [3, 2, 1, 3, 2, 1])
    [0, 3]
    >>> indices(4, [1, 2, 3])
    []
    >>> indices(10, [2, 10, 3, 10, 4, 10, 5])
    [1, 3, 5]
    ```

{{ IDE('exo', SANS="index, max") }}

