{{ IDE('exo_corr') }}

On aurait pu utiliser les méthodes `.keys()`, `.values()` et `.items()`  
  
Voici une autre proposition de résolution en utilisant ces méthodes :  

```python
def scores_aimes(votes):
    """
    Renvoie le dictionnaire dont les clés sont les personnes,
    et les valeurs le nombre de personnes qui leur ont attribué des J'aime
    """
    bilan = {}
    for personne in votes.keys(): # ou tout simplement for personne in votes
        bilan[personne] = 0
    for liste_choisis in votes.values():
        for prenom in liste_choisis:
            bilan[prenom] = bilan[prenom] + 1
    return bilan


def gagnants(votes):
    """
    Renvoie la liste des personnes ayant le plus reçu de J'aime
      triée par ordre alphabétique
    """
    bilan = scores_aimes(votes)
    maximum = max(bilan.values())
    liste_gagnants = [nom for (nom, score) in bilan.items() if score == maximum]
    return liste_gagnants
```

