def scores_aimes(votes):
    """
    Renvoie le dictionnaire dont les clés sont les personnes,
    et les valeurs le nombre de personnes qui leur ont attribué des J'aime
    """
    bilan = {}
    for personne in votes:
        bilan[personne] = 0

    for personne in votes:
        for personne_choisie in votes[personne]:
            bilan[personne_choisie] = bilan[personne_choisie] + 1
    return bilan


def gagnants(votes):
    """
    Renvoie la liste des personnes ayant le plus reçu de : J'aime
    """
    bilan = scores_aimes(votes)
    maxi = 0
    for personne in bilan:
        if bilan[personne] > maxi:
            maxi = bilan[personne]

    liste_gagnants = [nom for nom in bilan if bilan[nom] == maxi]

    return liste_gagnants


# Tests
votes_soiree = {
    "Alice": ["Bob", "Carole", "Dylan"],
    "Bob": ["Carole", "Esma"],
    "Esma": ["Bob", "Alice"],
    "Fabien": ["Dylan"],
    "Carole": [],
    "Dylan": [],
}


assert scores_aimes(votes_soiree) == {
    "Bob": 2,
    "Alice": 1,
    "Esma": 1,
    "Fabien": 0,
    "Carole": 2,
    "Dylan": 2,
}

assert sorted(gagnants(votes_soiree)) == ["Bob", "Carole", "Dylan"]
