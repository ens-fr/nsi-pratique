HEX_DEC = {'0':0, '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9,
           'A':10, 'B':11, 'C':12, 'D':13, 'E':14, 'F':15}

def hex_int(seizaine, unite):
    """seizaine et unite sont les chiffres de l'écriture d'un 
    nombre en base 16 à 2 chiffres. Renvoie la valeur int en base 10
    """
    return HEX_DEC[seizaine] * 16 + HEX_DEC[unite]

def html_vers_rvb(html):
    """html: str représente une couleur HTML
    renvoie le triplet représentant la couleur en RVB
    """
    return hex_int(html[1], html[2]), hex_int(html[3], html[4]), hex_int(html[5], html[6])
    

# test

assert hex_int('B', '5') == 181, "Échec hex_int exemple 1 de l'énoncé"
assert hex_int('0', '0') == 0, "Échec hex_int exemple 2 de l'énoncé"

assert html_vers_rvb("#C0392B") == (192, 57, 43), "Échec html_vers_rvb exemple 1"
assert html_vers_rvb("#00FF00") == (0, 255, 0), "Échec html_vers_rvb exemple 2"
assert html_vers_rvb("#000000") == (0, 0, 0), 'Échec html_vers_rvb exemple 3'

