# Tests
animaux = [{'nom': 'Medor', 'espece': 'chien', 'age': 5, 'enclos': 2},
           {'nom': 'Titine', 'espece': 'chat', 'age': 2, 'enclos': 5},
           {'nom': 'Tom', 'espece': 'chat', 'age': 7, 'enclos': 4},
           {'nom': 'Belle', 'espece': 'chien', 'age': 6, 'enclos': 3},
           {'nom': 'Mirza', 'espece': 'chat', 'age': 6, 'enclos': 5}]

assert selection_enclos(animaux, 5) == [{'nom': 'Titine', 'espece': 'chat', 'age': 2, 'enclos': 5}, {
    'nom': 'Mirza', 'espece': 'chat', 'age': 6, 'enclos': 5}]
assert selection_enclos(animaux, 2) == [
    {'nom': 'Medor', 'espece': 'chien', 'age': 5, 'enclos': 2}]
assert selection_enclos(animaux, 7) == []

# Tests supplémentaires
enclos = {animal['enclos'] for animal in animaux}
for num_enclos in enclos:
    reponse = [ani for ani in animaux if ani['enclos'] == num_enclos]
    assert selection_enclos(animaux, num_enclos) == reponse

assert selection_enclos(animaux, -1) == []

animaux = [{'nom': 'Moder', 'espece': 'lapin', 'age': 5, 'enclos': 0},
           {'nom': 'Ollie', 'espece': 'furet', 'age': 6, 'enclos': 0}]
assert selection_enclos(animaux, 0) == animaux
assert selection_enclos(animaux, 1) == []
