---
author: Nicolas Revéret
title: Le bon enclos
tags:
  - 3-dictionnaire
---

# Le bon enclos

On considère une table (une liste de dictionnaires Python) qui contient des enregistrements relatifs à des animaux hébergés dans un refuge.

Les attributs des enregistrements sont :

* `nom`,
* `espece`,
* `age`,
* `enclos`.
 
Les valeurs associées à `nom` et `espece` sont des chaines de caractères, celles associées à `age` et `enclos` des entiers.

 Voici un exemple d'une telle table :

```python
animaux = [ {'nom': 'Medor', 'espece': 'chien', 'age': 5, 'enclos': 2},
            {'nom': 'Titine', 'espece': 'chat', 'age': 2, 'enclos': 5},
            {'nom': 'Tom', 'espece': 'chat', 'age': 7, 'enclos': 4},
            {'nom': 'Belle', 'espece': 'chien', 'age': 6, 'enclos': 3},
            {'nom': 'Mirza', 'espece': 'chat', 'age': 6, 'enclos': 5}]
```

On garantit que chaque enregistrement contient l'ensemble des informations (aucune clé ne manque dans un dictionnaire).

Programmer une fonction `selection_enclos` qui :

* prend en paramètres :
    * une liste `table_animaux` contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
    * un numéro d'enclos `num_enclos` ;
* renvoie une table contenant les enregistrements de `table_animaux` dont l'attribut `enclos` est égal à `num_enclos`. Dans cette table, les enregistrements seront donnés dans le même ordre que dans la table d'entrée.


Exemples avec la table animaux ci-dessus :

!!! example "Exemples"

    ```pycon
    >>> selection_enclos(animaux, 5)
    [{'nom': 'Titine', 'espece': 'chat', 'age': 2, 'enclos': 5},
    {'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
    >>> selection_enclos(animaux, 2)
    [{'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2}]
    >>> selection_enclos(animaux, 7)
    []
    ```

{{ IDE('exo') }}
