## Commentaires

{{ IDE('exo_corr') }}

Le problème revient à filtrer les éléments d'une liste de dictionnaires selon un critère portant sur les valeurs des dictionnaires.

On crée une liste `resultat` qui contiendra les enregistrements souhaités.

On parcourt ensuite la liste des enregistrements avec la variable nommée `animal`. Pour chaque `animal` on teste son enclos. S'il correspond à celui cherché, on ajoute l'enregistrement à la fin de la liste.

