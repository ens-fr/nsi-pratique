# Commentaires

Une première approche consiste à simplifier l'adresse à tester en supprimant les `.`. On recopie ainsi les caractères valides jusqu'à rencontrer le `@`. Après celui-ci, on recopie la fin de l'adresse sans modification.

Cette étape de simplification effectuée on peut renvoyer le résultat de la comparaison entre l'adresse « simplifiée » et l'adresse de référence.

Le code ci-dessous procède ainsi :

```python
def correspond(adresse, reference):
    adresse_simplifiee = ""
    i = 0
    while adresse[i] != "@":
        if adresse[i] != ".":
            adresse_simplifiee = adresse_simplifiee + adresse[i]
        i = i + 1
    for j in range(i, len(adresse)):
        adresse_simplifiee = adresse_simplifiee + adresse[j]
    return adresse_simplifiee == reference
```

Parfaitement fonctionnelle, cette méthode présente toutefois un petit désavantage : l'adresse à tester est lue deux fois. Une fois lors de la simplification et une seconde lors de la comparaison.

La méthode présentée ci-dessous permet de comparer les adresses en une seule « passe ».

{{ IDE('exo_corr') }}

Le traitement des adresses diffère selon que l'on est dans la partie *locale* ou *domaine*. Le booléen `local` vaut `True` dans le premier cas, `False` sinon.

Dans la partie *locale* on ignore les `"."` en passant au caractère suivant dans l'adresse à tester. 

Si le caractère lu dans l'adresse à tester n'est pas un point et diffère du caractère correspondant dans l'adresse de référence on renvoie `False`, quelle que soit la partie étudiée.

Dans le dernier cas de figure, les caractères sont identiques. S'il s'agit du `"@"`, on bascule la valeur de `local` à `False`. On peut alors passer aux caractères suivants dans chaque adresse.

À la fin de la fonction on vérifie que l'on est arrivé à la fin de chaque adresse.

Notons qu'il est possible de résoudre cet exercice en utilisant une [expression régulière](https://regex101.com/r/m6MYC2/1) :

```python
import re

def correspond(adresse, reference):
    adresse = "".join(re.findall(r"@.+|[^.]+(?=@)|[^.]+", adresse))
    return adresse == reference
```

Cette expression régulière ne permet toutefois pas de tester la conformité d'une adresse électronique. Voir [cette page](https://www.ex-parrot.com/~pdw/Mail-RFC822-Address.html) pour une proposition d'expression régulière validant les adresses électroniques.