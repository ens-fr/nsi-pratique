# Tests
assert correspond("courspremiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours.premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("co.ur.spremi.ere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours..premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond(".courspremiere.@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours_premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == False
assert correspond("courspremier@e-nsi.fr", "courspremiere@e-nsi.fr") == False
assert correspond("courspremiere@aeif.org", "courspremiere@e-nsi.fr") == False



# Tests supplémentaires
assert not correspond("spe_isn@serveur.org", "spe_nsi@serveur.org")
assert not correspond("specialite_nsi@serveur.org", "spe_nsi@serveur.org")
assert correspond("..spe_nsi@serveur.org", "spe_nsi@serveur.org")
assert correspond("spe_nsi..@serveur.org", "spe_nsi@serveur.org")


import string, random

carcateres = string.ascii_lowercase + string.digits[:-2] + "-_"
serveur_A = "@e-nsi.fr"
serveur_B = "@e-nsi.de"
serveur_C = "@e-nsi.fra"
for _ in range(100):
    locale_ref = [random.choice(carcateres) for _ in range(10)]
    reference = "".join(locale_ref) + serveur_A
    locale_adr = locale_ref.copy()
    for nb_points in range(random.randrange(0, 5)):
        position = random.randrange(0, len(locale_adr))
        locale_adr.insert(position, ".")

    # Identiques
    adresse = "".join(locale_adr) + serveur_A
    assert correspond(adresse, reference), f"Erreur avec {adresse} et {reference}"
    
    # Serveurs différents
    adresse = "".join(locale_adr) + serveur_B
    assert not correspond(adresse, reference), f"Erreur avec {adresse} et {reference}"
    
    # Serveurs différents (plus long)
    adresse = "".join(locale_adr) + serveur_C
    assert not correspond(adresse, reference), f"Erreur avec {adresse} et {reference}"
    
    # Partie locale différente
    adresse = "".join(locale_ref)[:-2] + "89" + serveur_A
    assert not correspond(adresse, reference), f"Erreur avec {adresse} et {reference}"
    
    # Partie locale plus courte
    adresse = "".join(locale_ref)[:-2] + serveur_A
    assert not correspond(adresse, reference), f"Erreur avec {adresse} et {reference}"
    
    # Partie locale plus longue
    adresse = "".join(locale_ref) + "".join(locale_ref)[:2] + serveur_A
    assert not correspond(adresse, reference), f"Erreur avec {adresse} et {reference}"
