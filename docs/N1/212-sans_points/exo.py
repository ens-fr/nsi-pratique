def correspond(adresse, reference):
    ...


# Tests
assert correspond("courspremiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours.premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("co.ur.spremi.ere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours..premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond(".courspremiere.@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours_premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == False
assert correspond("courspremier@e-nsi.fr", "courspremiere@e-nsi.fr") == False
assert correspond("courspremiere@aeif.org", "courspremiere@e-nsi.fr") == False
