## Commentaires

### Itération sans indice

Version recommandée

{{ IDE('exo_corr') }}

### Itération avec indice

Version possible ; peu d'intérêt

```python
def compte_occurrences(cible, mot):
    nb_occurrences = 0
    for i in range(len(mot)):
        if mot[i] == cible:
            nb_occurrences += 1
    return nb_occurrences
```

### Version fonctionnelle

Pour les bons élèves ; n'est pas hors programme.

```python
def compte_occurrences(cible, mot):
    return sum(1 for lettre in mot if lettre == cible)
```

### Version non autorisée

Avec la facilité du langage Python (`count`).

```python
def compte_occurrences(cible, mot):
    return mot.count(cible)
```
