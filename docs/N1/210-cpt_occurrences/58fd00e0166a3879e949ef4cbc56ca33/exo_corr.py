def compte_occurrences(cible, mot):
    nb_occurrences = 0
    for lettre in mot:
        if lettre == cible:
            nb_occurrences += 1
    return nb_occurrences


# tests
assert compte_occurrences("o", "bonjour") == 2
assert compte_occurrences("a", "abracadabra") == 5
assert compte_occurrences("i", "abracadabra") == 0
