---
author: Franck Chambon
title: Triominos et pavage (2)
tags:
  - 4-grille
  - 5-diviser/régner
  - 6-récursivité
---

# Pavage possible avec triominos (2)

![](images/pavage.svg)

Dans cet exercice, on considère un carré (avec un trou) dans une grille, dont le côté est une puissance de 2, et des triominos pour le paver. Il est recommandé d'avoir fait l'exercice [Triominos et pavage](../490-pavage_triomino/sujet.md) ; on utilisera le même codage des triominos.

![](images/triominos.svg)

!!! abstract "Objectif"
    On souhaite obtenir un pavage d'un carré troué par des triominos.

    Écrire une fonction `pavage_triominos` qui prend en paramètres

    - `n` : le côté d'un carré ; **`n` sera une puissance de 2.**
    - `i_trou` : la ligne du trou
    - `j_trou` : la colonne du trou
    
    La fonction doit renvoyer une liste de triominos qui pave le carré troué. Chaque triomino est codé avec un tuple `(i, j, sens)` tel que défini dans l'exercice précédent ; `i` et `j` désignent la ligne et la colonne du carré central du triomino, et `sens` indique son orientation.
    
    S'il est impossible d'obtenir un pavage, la fonction renverra `#!py None`.

    Dans cet exercice, on fournit et il sera possible d'utiliser la fonction `est_pavage` de l'exercice précédent pour vérifier votre pavage ; il n'est souvent pas unique. Tout pavage valide sera accepté.

!!! example "Exemples"
    Premier exemple, ici le pavage est unique.

    ![](images/ex2.svg)
    ```pycon
    >>> pavage_triominos(2, 1, 1)
    [(0, 0, 3)]
    >>> est_pavage(2, 1, 1, pavage_triominos(2, 1, 1))
    True
    ```

    Deuxième exemple, ici le pavage n'est pas unique.

    ![](images/ex4.svg)
    ```pycon
    >>> pavage_triominos(4, 0, 1)
    [(0, 3, 2), (1, 0, 1), (2, 2, 0), (3, 0, 1), (3, 3, 0)]
    >>> est_pavage(4, 0, 1, pavage_triominos(4, 0, 1))
    True
    ```

    Troisième exemple, ici le pavage n'est pas unique.

    ![](images/pavage.svg)
    ```pycon
    >>> est_pavage(8, 5, 4, pavage_triominos(8, 5, 4))
    True
    ```


{{ IDE('exo', MAX=1000) }}


??? tip "Indice 1"
    Si le carré est une puissance de deux, on peut le couper en 4.

    - le petit carré qui contient le trou peut être paver par récursivité.
    - pour les trois autres, on place un triomino commun, de sorte qu'il reste à paver un petit carré qui possède un trou à un coin.

    ![](images/indice.svg)

    ![](images/indice2.svg)



??? tip "Indice 2"
    Pour paver les trois autres carrés avec un trou dans un coin, on peut utiliser de la récursivité et un décalage sur le pavage obtenu.

    Écrire chaque cas pourra s'avérer un peu long. Mais il est possible de factoriser le code, étudier le corrigé en ce sens.

??? tip "Indice 3"
    On pourra utiliser le squelette de code

    ```python
    def pavage_triominos(n, i_trou, j_trou):
        resultat = []
        if n == 1:
            return ...
        m = n // 2
        if i_trou < m:  # le trou est en haut
            if j_trou < m:  # le trou est à gauche
                ...
            else:           # le trou est à droite
                ...
        else:           # le trou est en bas
            ...

        ...
        return resultat
    ```
