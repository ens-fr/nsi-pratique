---
author: Franck Chambon
title: Somme fixée
tags:
  - 9-prog.dynamique
---
# Somme fixée de deux valeurs d'un tableau trié

On dispose d'un tableau d'entiers triés dans l'ordre croissant, mais on ne dispose pas d'accès à la valeur d'un élément, on dispose de la somme de deux éléments d'indices distincts donnés. L'objectif est de savoir si on peut obtenir une somme donnée avec deux indices distincts.

!!! note "Deux exemples"
    1. Avec le tableau $3, 4, 6, 9, 13, 18, 19$, il est impossible d'obtenir $20$ comme somme de deux éléments distincts.
    2. Avec le tableau $3, 5, 7, 8, 9, 12, 16, 19$, il est possible d'obtenir $21$ comme somme de deux éléments distincts.

Vous disposez :

- d'une fonction `taille()` qui renvoie la taille du tableau trié considéré ;
- d'une fonction telle que `somme(i, j)` renvoie la somme de deux éléments distincts d'un tableau. **Attention**, on veillera à ce que `0 <= i < j < taille()` sinon une erreur se produira. D'autre part, il ne sera autorisé d'utiliser la fonction `somme` qu'au maximum `taille()` fois, et vous ne connaissez pas la structure cachée derrière `somme`, ni `taille`.

Écrire une fonction telle que `est_somme(x)` renvoie un booléen qui détermine si `x` peut être la somme de deux éléments distincts du tableau.

!!! example "Exemples"
    Avec l'exemple 1 précédent :
    ```pycon
    >>> taille()
    7
    >>> somme(0, 2)
    9
    >>> est_somme(20)
    False
    ```
    
    Avec l'exemple 2 précédent :
    ```pycon
    >>> taille()
    8
    >>> somme(0, 2)
    10
    >>> est_somme(21)
    True
    ```
    

{{ IDE('exo', MAX=1000) }}
