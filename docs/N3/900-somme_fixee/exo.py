def est_somme(x):
    ...




# tests

# test 1
tableau_1 = [3, 4, 6, 9, 13, 18, 19]

def taille():
    return len(tableau_1)

nb_tests = 0
def somme(i, j):
    global nb_tests
    assert 0 <= i < j < taille(), "Mauvais indices"
    nb_tests += 1
    correct = nb_tests <= taille()
    assert correct, "Il faut utiliser `somme` moins souvent"
    return tableau_1[i] + tableau_1[j]

assert est_somme(20) == False

# test 2
tab_2 = [3, 5, 7, 8, 9, 12, 16, 19]

def taille():
    return len(tab_2)

nb_s = 0
def somme(i, j):
    global nb_s
    assert 0 <= i < j < taille(), "Mauvais indices"
    nb_s += 1
    correct = nb_s <= taille()
    assert correct, "Il faut utiliser `somme` moins souvent"
    return tab_2[i] + tab_2[j]

assert est_somme(21) == True

