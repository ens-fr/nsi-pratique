def somme_maxi(valeurs, k):
    ...


# tests

assert somme_maxi([0, 1, 2, 3, 2, 1, 0], 3) == 7
assert somme_maxi([0, 1, 2, 3, 2, 1, 0], 1) == 3
