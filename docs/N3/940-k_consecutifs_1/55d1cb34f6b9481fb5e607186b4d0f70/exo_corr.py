def somme_maxi(valeurs, k):
    k_somme = 0
    for i in range(k):
        k_somme += valeurs[i]
    maxi = k_somme
    for i in range(k, len(valeurs)):
        k_somme += valeurs[i] - valeurs[i - k]
        if k_somme > maxi:
            maxi = k_somme
    return maxi



# tests

assert somme_maxi([0, 1, 2, 3, 2, 1, 0], 3) == 7
assert somme_maxi([0, 1, 2, 3, 2, 1, 0], 1) == 3
