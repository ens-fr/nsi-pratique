---
author: Franck Chambon
title: k-somme maximale
tags:
  - 9-prog.dynamique
  - 4-maths
---
# Somme maximale de k termes consécutifs

Écrire une fonction telle que `somme_maxi(valeurs, k)` renvoie la somme maximale de `k` termes consécutifs dans une liste de `valeurs`.

> On garantit que `valeurs` est de taille au moins égale à `k` et que `k > 0`.

!!! example "Example"
    ```pycon
    >>> somme_maxi([0, 1, 2, 3, 2, 1, 0], 3)
    7
    >>> somme_maxi([0, 1, 2, 3, 2, 1, 0], 1)
    3
    ```

{{ IDE('exo', MAX=1000) }}


??? tip "Indice 1"
    On pourra commencer par faire le cumul des `k` premières valeurs pour initialiser une variable `maxi`.

    On pourra ensuite faire une boucle qui ajoute la valeur suivante et retranche la première valeur.

??? tip "Indice 2"
    On pourra s'aider du code à trou

    ```python
    def somme_maxi(valeurs, k):
        k_somme = ...
        for i in range(k):
            k_somme += ...
        maxi = k_somme
        for i in range(..., ...):
            k_somme += valeurs[...] - valeurs[...]
            if k_somme > maxi:
                maxi = ...
        return ...
    ```
