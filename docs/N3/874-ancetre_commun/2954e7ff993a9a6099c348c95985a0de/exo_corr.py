def ancetres(parents, noeud):
    ancetres_noeud = [noeud]
    while noeud is not None:
        noeud = parents[noeud]
        ancetres_noeud.append(noeud)
    return ancetres_noeud

def plus_petit_ancetre_commun(parents, noeud_a, noeud_b):
    ancetres_a = ancetres(parents, noeud_a)
    ancetres_b = ancetres(parents, noeud_b)

    while (ancetres_a != []) and (ancetres_b != [])\
        and (ancetres_a[-1] == ancetres_b[-1]):
            ppac = ancetres_a.pop()
            ancetres_b.pop()
    return ppac



# tests

parents = [None, 0, 0, 0, 2, 2, 2, 5, 5, 6, 6, 9, 9]
assert plus_petit_ancetre_commun(parents, 8, 11) == 2
assert plus_petit_ancetre_commun(parents, 1, 10) == 0
assert plus_petit_ancetre_commun(parents, 12, 6) == 6
