def plus_petit_ancetre_commun(parents, noeud_a, noeud_b):
    ...



# tests

parents = [None, 0, 0, 0, 2, 2, 2, 5, 5, 6, 6, 9, 9]
assert plus_petit_ancetre_commun(parents, 8, 11) == 2
assert plus_petit_ancetre_commun(parents, 1, 10) == 0
assert plus_petit_ancetre_commun(parents, 12, 6) == 6
