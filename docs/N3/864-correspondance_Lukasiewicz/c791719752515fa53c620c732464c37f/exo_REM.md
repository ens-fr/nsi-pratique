## Commentaires

{{ IDE('exo_corr') }}

### Culture

> [Łukasiewicz](https://fr.wikipedia.org/wiki/Jan_%C5%81ukasiewicz) a [...] jeté les bases de l'informatique moderne en créant la notation polonaise et en développent avec Alfred Tarski et Stefan Banach les fondations de la notation inverse, une manière d'écrire des expressions arithmétiques largement utilisées à ce jour en informatique.

