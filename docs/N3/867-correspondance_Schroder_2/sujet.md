---
author: Franck Chambon
title: Correspondance Schröder (2)
tags:
  - 8-arbre
  - 4-chemin
  - 6-récursivité
  - 7-pile
---
# Correspondance de Schröder (2)

!!! info "Suite de l'exercice précédent"
    Dans l'exercice précédent, on a construit une fonction qui renvoie un chemin pour un arbre de Schröder donné.

    Ici, il s'agit de la fonction réciproque, ce qui établit une correspondance entre arbre de Schröder et chemin de Schröder.


L'objectif de l'exercice est de construire une fonction telle que `chemin_vers_arbre(chemin)` renvoie la description d'un arbre de Schröder du `chemin` passé en paramètre avec sa description.

!!! example "Exemples"

    - ![](images/chemin_simple_1.svg) donne ![](images/arbre_simple_1.svg)

    ```pycon
    >>> chemin_vers_arbre([(1, 1), (1, 1), (1, -1), (2, 0), (1, -1)])
    [[[], []], [], []]
    ```

    - ![](images/chemin_simple_2.svg) donne ![](images/arbre_simple_2.svg)

    ```pycon
    >>> chemin_vers_arbre([(1, 1), (2, 0), (1, 1), (1, -1), (1, -1)])
    [[], [[], []], []]
    ```

    - ![](images/chemin_joli.svg) donne ![](images/arbre_joli.svg)

    ```pycon
    >>> chemin_vers_arbre([(1, 1), (2, 0), (1, 1), (2, 0), (1, 1), (1, -1), (2, 0), (2, 0), (1, -1), (1, -1)])
    [[], [[], [[], []], [], [], []], []]
    ```

{{ IDE('exo', MAX=1000) }}

??? tip "Indice 1"
    On pourra créer une fonction telle que `ajout_feuille_droite(arbre, niveau)` ajoute une feuille à `arbre` totalement à droite, à la profondeur `niveau`.

??? tip "Indice 2"
    On pourra utiliser une pile qui recense les ouvertures de niveau.

??? tip "Indice 3"
    Cet exercice n'est pas simple du tout. Bon courage !!!
