def chemin_vers_arbre(chemin):
    arbre = []
    noeud = arbre
    pile = []
    for dx, dy in chemin:
        if dy == +1:
            pile.append(noeud)
        else:
            noeud = pile[-1]
            if dy == -1:
                pile.pop()
        noeud.append([])
        noeud = noeud[-1]
    return arbre

# tests

assert chemin_vers_arbre(
    [(1, 1), (1, 1), (1, -1), (2, 0), (1, -1)]
) == [[[], []], [], []]

assert chemin_vers_arbre(
    [(1, 1), (2, 0), (1, 1), (1, -1), (1, -1)]
) == [[], [[], []], []]

assert chemin_vers_arbre(
    [(1, 1), (2, 0), (1, 1), (2, 0), (1, 1), (1, -1), (2, 0), (2, 0), (1, -1), (1, -1)]
) == [[], [[], [[], []], [], [], []], []]



