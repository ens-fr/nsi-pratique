# 🗒️ Présentation

Dans cette catégorie, on trouve des exercices plus ou moins complexes qui intéresseront les élèves qui veulent aller, ou qui sont déjà, en MP2I. Ils sont guidés d'abord avec des indices, voire avec des morceaux de code.

Ces exercices permettent de gagner en autonomie progressivement. 

![](./Ada_lovelace.jpg)

> [Ada Lovelace](https://fr.wikipedia.org/wiki/Ada_Lovelace), comtesse de Lovelace, née Ada Byron le 10 décembre 1815 à Londres et morte le 27 novembre 1852 à Marylebone dans la même ville, est une pionnière de la science informatique.
>
> Elle est principalement connue pour avoir réalisé le premier véritable programme informatique, lors de son travail sur un ancêtre de l'ordinateur : la machine analytique de Charles Babbage. Dans ses notes, on trouve en effet le premier programme publié, destiné à être exécuté par une machine, ce qui fait considérer Ada Lovelace comme « le premier programmeur du monde ». Elle a également entrevu et décrit certaines possibilités offertes par les calculateurs universels, allant bien au-delà du calcul numérique et de ce qu'imaginaient Babbage et ses contemporains.
