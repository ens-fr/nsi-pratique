# tests

## exemples
assert est_tas_min([None, 10, 42, 23, 55, 67]) == True

assert est_tas_min([None, 'brrr', 'chut', 'ouille', 'dring', 'tada', 'vroum', 'wahou', 'paf', 'hehe']) == True

assert est_tas_min([None]) == True

assert est_tas_min([None, 10, 10]) == True

## contre-exemples
assert est_tas_min([None, 10, 2]) == False

assert est_tas_min([None, 'ba', 'ab']) == False

assert est_tas_min([None, 10, 42, 23, 30]) == False

assert est_tas_min([None, 10, 42, 23, 55, 40]) == False


# autres tests

## exemples
assert est_tas_min([None, 110, 142, 123, 155, 167]) == True

assert est_tas_min([None, 'zbrrr', 'zchut', 'zouille', 'zdring', 'ztada', 'zvroum', 'zwahou', 'zpaf', 'zhehe']) == True

assert est_tas_min([None, 100.0, 100.0]) == True


assert est_tas_min([None, 110, 142, 123, 155, 167, 125, 123, 156, 157]) == True
assert est_tas_min([None, 110, 142, 123, 155, 167, 125, 123, 156, 157, 168]) == True

## contre-exemples
assert est_tas_min([None, 110, 12]) == False

assert est_tas_min([None, 'zba', 'zab']) == False

assert est_tas_min([None, 110, 142, 123, 130]) == False

assert est_tas_min([None, 110, 142, 123, 155, 140]) == False

## lourds
lourd = [None]
lourd.extend(range(10**5))
assert est_tas_min(lourd)

lourd = [None]
lourd.extend(range(10**5))
lourd[42] = 0
assert not est_tas_min(lourd)
