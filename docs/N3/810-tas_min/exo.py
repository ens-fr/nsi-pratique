def est_tas_min(tableau):
    ...





# tests

## exemples
assert est_tas_min([None, 10, 42, 23, 55, 67]) == True

assert est_tas_min([None, 'brrr', 'chut', 'ouille', 'dring', 'tada', 'vroum', 'wahou', 'paf', 'hehe']) == True

assert est_tas_min([None]) == True

assert est_tas_min([None, 10, 10]) == True

## contre-exemples
assert est_tas_min([None, 10, 2]) == False

assert est_tas_min([None, 'ba', 'ab']) == False

assert est_tas_min([None, 10, 42, 23, 30]) == False

assert est_tas_min([None, 10, 42, 23, 55, 40]) == False
