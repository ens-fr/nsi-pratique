## Commentaires

{{ IDE('exo_corr') }}

$n+1$ est bien la taille de `tableau`, les indices pour les éléments (hors `None`) sont de $1$ à $n$ inclus.

Les indices des éléments qui possèdent un ancêtre sont de $2$ inclus à $n+1$ exclu.

Il suffit juste de vérifier que chaque ancêtre n'est pas supérieur.
