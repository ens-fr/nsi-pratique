def est_tas_min(tableau):
    n = len(tableau) - 1
    for i in range(2, n + 1):
        if tableau[i//2] > tableau[i]:
            return False
    return True



# tests

## exemples
assert est_tas_min([None, 10, 42, 23, 55, 67]) == True

assert est_tas_min([None, 'brrr', 'chut', 'ouille', 'dring', 'tada', 'vroum', 'wahou', 'paf', 'hehe']) == True

assert est_tas_min([None]) == True

## contre-exemples
assert est_tas_min([None, 10, 2]) == False

assert est_tas_min([None, 'ba', 'ab']) == False

assert est_tas_min([None, 10, 42, 23, 30]) == False

assert est_tas_min([None, 10, 42, 23, 55, 40]) == False
