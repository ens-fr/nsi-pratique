---
author: Franck Chambon
title: Triominos et pavage (1)
tags:
  - 4-grille
---

# Pavage possible avec triominos (1)

En utilisant uniquement 3 carrés collés sur un côté en forme de L (un triomino), on a 4 sens possibles d'orientation si on souhaite garder les côtés parallèles aux axes.

![triominos](images/triominos.svg)

On peut alors paver, par exemple, un carré de côté 8, percé à la case ligne 5, colonne 4. On a utilisé 21 triominos.

![triominos](images/pavage.svg)

!!! abstract "Objectif"
    On souhaite paver presque entièrement une grille carrée de $n$ lignes et $n$ colonnes, indicées de $0$ inclus à $n$ exclu. Il y a juste un trou à la ligne `i_trou`, colonne `j_trou` qui n'est pas à paver. Il ne faut pas paver en dehors de la grille carrée.

    Écrire une fonction `est_pavage` qui détermine si un pavage est correct ou non. On donne en paramètres :

    - `n` : le côté du carré
    - `i_trou` : la ligne du trou
    - `j_trou` : la colonne du trou
    - `triominos` : la liste des triominos

    Un triomino est donné avec un tuple, `(i, j, sens)`

    - `i` désigne la ligne du carré central
    - `j` désigne la colonne du carré central
    - `sens` est l'orientation, selon le code indiqué plus haut ; un entier de `0` inclus à `4` exclu.
    
    Par exemple, dans l'exemple au-dessus
    
    - `(7, 7, 0)` désigne le triomino vert en bas à droite
    - `(7, 0, 1)` désigne le triomino rouge en bas à gauche
    - `(0, 1, 2)` désigne le triomino jaune en haut à gauche
    - `(6, 3, 3)` désigne le triomino bleu en bas au milieu
    

!!! example "Exemples"

    Un pavage valide d'un carré de côté 2 avec un trou en $(1, 1)$.
    
    ![](images/ex2.svg)
    ```pycon
    >>> n = 2
    >>> i_trou, j_trou = 1, 1
    >>> triominos = [(0, 0, 3)]
    >>> est_pavage(n, i_trou, j_trou, triominos)
    True
    ```

    Un pavage invalide (incomplet) d'un carré de côté 3 avec un trou en $(1, 2)$.
     
    ![](images/ex3.svg)
    ```pycon
    >>> n = 3
    >>> i_trou, j_trou = 1, 2
    >>> triominos = [(0, 0, 3), (2, 1, 0)]
    >>> est_pavage(n, i_trou, j_trou, triominos)
    False
    ```

    Un pavage invalide d'un carré de côté 4 avec un trou en $(1, 0)$ ; il y a un chevauchement en $(2, 2)$.
    
    ![](images/ex4.svg)
    ```pycon
    >>> n = 4
    >>> i_trou, j_trou = 1, 0
    >>> triominos = [(0, 1, 2), (2, 1, 2), (0, 2, 3), (2, 3, 0), (3, 2, 1)]
    >>> est_pavage(n, i_trou, j_trou, triominos)
    False
    ```

    Un pavage valide.
    
    ![](images/ex5.svg)
    ```pycon
    >>> n = 5
    >>> i_trou, j_trou = 4, 0
    >>> triominos = [(0, 0, 3), (0, 2, 3), (1, 4, 0), (2, 1, 1), (2, 3, 3), (3, 0, 1), (4, 2, 0), (4, 4, 0)]
    >>> est_pavage(n, i_trou, j_trou, triominos)
    True
    ```

{{ IDE('exo', MAX=1000) }}
