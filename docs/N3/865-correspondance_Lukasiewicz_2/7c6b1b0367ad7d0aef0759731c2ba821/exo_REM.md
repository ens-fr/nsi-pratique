## Commentaires

### Version récursive

{{ IDE('exo_corr') }}

### Version itérative

On utilise ici deux piles

```python
def chemin_vers_arbre(chemin):
    "correspondance de Łukasiewicz"
    arbre = [[]]
    while chemin:
        sous_arbre = []
        for _ in range(1 + chemin.pop()) :
            sous_arbre.append(arbre.pop())
        arbre.append(sous_arbre)
    return arbre.pop()
```

On initialise `arbre` avec la dernière feuille `[]` qui vient à la fin du codage.

`arbre` se construit comme une pile de sous-arbres, qui sont extraits pour fabriquer un nouveau sous-arbre plus grand. À la fin `arbre` ne contient qu'un élément : l'arbre en question.
