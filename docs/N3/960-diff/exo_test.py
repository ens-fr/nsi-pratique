# Tests supplémentaires
texte_a = "bertrand roule en vélo"
texte_b = "bravo"
assert lplsc(texte_a, texte_b) == 5


texte_a = "resulta"
texte_b = "résultats"
assert lplsc(texte_a, texte_b) == 6


texte_a = "résultats"
texte_b = "résultats"
assert lplsc(texte_a, texte_b) == 9


texte_a = "résultats"
texte_b = ""
assert lplsc(texte_a, texte_b) == 0
