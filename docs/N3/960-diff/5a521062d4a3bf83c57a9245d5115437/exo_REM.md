## Commentaires

{{ IDE('exo_corr') }}

Dans la fonction principale on détermine la longueur de chaque chaine avant de créer le dictionnaire `memoire` initialement vide. On initie ensuite les appels récursifs en faisant `lplsc_rec(i_a, i_b)`.

Comme la sous-fonction est récursive, il est bon de débuter par un cas de base. C'est ici le cas ou l'un des deux textes est de longueur nulle. On teste ce cas en observant le rang du dernier caractère à étudier : s'il est négatif, c'est que la chaine est vide (on a soustrait `1` une fois de trop à `i_a` ou `i_b`). Dans ce cas, on renvoie directement 0.

On vérifie ensuite que le cas a été traité ou non. Pour cela on se demande si la clé `(i_a, i_b)` n'est pas présente dans le dictionnaire `memoire`.

Si c'est le cas, il faut traiter cette comparaison :

* si les derniers caractères sont identiques, on passe aux caractères précédents et l'on ajoute 1 au résultat. Cette valeur est immédiatement stockée dans le dictionnaire `memoire`,
* si les caractères diffèrent, on étudie les deux cas précisés par l'énoncé. On place dans le dictionnaire le résultat le plus grand.

En dernier lieu, on renvoie la valeur que l'on vient d'insérer dans le dictionnaire.
