## Commentaires

{{ IDE('exo_corr') }}


Il y a plusieurs façons de construire le dictionnaire des ouvertures associées aux fermetures.

La version codée en dur qui n'est pas généralisable.


```python
ouverture = {')': '(', ']': '[', '}': '{', '>': '<'}
```

La première avec boucle :

```python
ouvrant = "([{<"
fermant = ")]}>"
ouverture = {}  # dictionnaire vide
for i in range(len(fermant)):
    ouverture[fermant[i]] = ouvrant[i]
```

La seconde par compréhension :

```python
ouvrant = "([{<"
fermant = ")]}>"
ouverture = {fermant[i]: ouvrant[i] for i in range(len(fermant))}
```

Sans les indices, avec un style fonctionnel, on zippe un élément de `fermant` avec celui associé de `ouvrant`, dans le même ordre.

```python
ouvrant = "([{<"
fermant = ")]}>"
ouverture = {f: o for f, o in zip(fermant, ouvrant)}
```

Ou bien en échangeant le rôle de `fermant` et `ouvrant` dans le zip, comme dans le corrigé tout en haut.


Enfin, il est même possible d'écrire

```python
ouverture = dict(zip(fermant, ouvrant))
```
