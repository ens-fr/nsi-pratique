def produit_maxi(valeurs, k):
    nb_zeros = 0
    produit = 1
    for i in range(k):
        x = valeurs[i]
        if x == 0:
            nb_zeros += 1
        else:
            produit *= x
    if nb_zeros == 0:
        maxi = produit
    else:
        maxi = 0
    
    for i in range(k, len(valeurs)):
        x = valeurs[i]
        if x == 0:
            nb_zeros += 1
        else:
            produit *= x
        
        x = valeurs[i - k]
        if x == 0:
            nb_zeros -= 1
        else:
            produit //= x
        
        if nb_zeros == 0 and produit > maxi:
            maxi = produit
    
    return maxi



# tests

assert produit_maxi([0, 1, 2, 3, 2, 1, 0], 3) == 12
assert produit_maxi([0, 1, 2, 3, 2, 1, 0], 1) == 3
