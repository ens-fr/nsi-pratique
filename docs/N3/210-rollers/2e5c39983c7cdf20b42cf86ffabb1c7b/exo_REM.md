# Commentaires

{{ IDE('exo_corr') }}

Une première approche consiste à :

* séparer les patins selon leur genre (gauche ou droit) ;
* trier chacune des deux listes obtenues ;
* s'assurer que les deux listes ont la même longueur. Dans le cas contraire on peut directement renvoyer `False` ;
* comparer les éléments de chaque liste dans l'ordre croissant : si les éléments de même indice dans chaque liste n'ont pas la même pointure ils ne peuvent pas être appariés et la fonction peut renvoyer `False`.

```python
def est_tas_valide(rollers):
    gauches = sorted([pointure for (genre, pointure) in rollers if genre == "gauche"])
    droits = sorted([pointure for (genre, pointure) in rollers if genre == "droit"])

    if len(gauches) != len(droits):
        return False

    for i in range(len(gauches)):
        if gauches[i] != droits[i]:
            return False

    return True
```

Cette approche présente néanmoins l'inconvénient de trier les deux listes. Cette étape est coûteuse en nombre d'opérations et peut être évitée.

On utilise donc autant de compteurs qu'il y a de pointures différentes et l'on parcourt l'ensemble des rollers. Pour chacun, s'il est du genre `"gauche"`, on incrémente le compteur de sa pointure, sinon on la décrémente. Ce fonctionnement ressemble 
à l'utilisation d'une pile : on empile les rollers « gauche », on dépile les « droit ».

En fin de parcours, on teste la nullité de chaque valeur de `paires`. Si l'une des valeurs est non nulle, on renvoie immédiatement `False`. À l'inverse si aucun des valeurs est non nulle on renvoie `True`.

Ce dernier parcours testant la nullité des tests peut être écrit plus rapidement avec : `return all(valeur == 0 for valeur in paires)`.

On utilise ici une liste afin de stocker les informations relatives à chaque pointure. Il serait aussi possible d'utiliser un dictionnaire :

```python
def est_tas_valide(rollers):
    paires = {pointure: 0 for pointure in range(36, 47)}
    for genre, pointure in rollers:
        if genre == "gauche":
            paires[pointure] += 1
        else:
            paires[pointure] -= 1

    for valeur in paires.values():
        if valeur != 0:
            return False
    return True
```

Notons enfin que cette approche consistant à « compter » le nombre de rollers de chaque pointure est utilisée dans une méthode de tri classique : [le tri par dénombrement (ou comptage)](https://fr.wikipedia.org/wiki/Tri_comptage).