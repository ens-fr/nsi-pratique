
# tests

grille = [[5, 2, 3], [2, 0, 5], [3, 2, 1]]
assert somme_minimale_2(grille) == 5


grille = [
    [9, 9, 0, 9, 9],
    [3, 1, 9, 9, 9],
    [5, 1, 9, 1, 3],
    [7, 1, 9, 1, 9],
    [9, 1, 1, 1, 9],
]
assert somme_minimale_2(grille) == 14


grille = [
    [131, 673, 234, 103, 18],
    [201, 96, 342, 965, 150],
    [630, 803, 746, 422, 111],
    [537, 699, 497, 121, 956],
    [805, 732, 524, 37, 331],
]
assert somme_minimale_2(grille) == 994

def secret_somme_minimale_2(grille):
    n = len(grille)
    colonne = [grille[i][0] for i in range(n)]
    for j in range(1, n):
        colonne[0] += grille[0][j]
        for i in range(1, n):
            colonne[i] = grille[i][j] + min(colonne[i-1], colonne[i])
        for i in range(n-2, -1, -1):
            colonne[i] = min(colonne[i], colonne[i+1] + grille[i][j])
    return min(colonne)

from random import randrange
for n in [1, 2, 3, 4, 5, 6, 7, 8, 16, 32, 64]:
    grille = [[randrange(10**6)*(i+3)*(j+3)*(n+3-i)*(n+3-j) for j in range(n)] for i in range(n)]
    attendu = secret_somme_minimale_2(grille)
    resultat = somme_minimale_2(grille)
    assert attendu == resultat
