---
author: project Euler 082, Franck CHAMBON
title: Somme minimale (2)
tags:
  - 4-grille
  - 9-prog.dynamique
---

# Somme minimale pour traverser un carré 

Une grille carrée étant donnée, remplie d'entiers positifs, on souhaite traverser le carré avec une somme minimale des entiers rencontrés.

Pour cet exercice,

- on souhaite aller du côté gauche au côté droit,
- les seuls déplacements autorisés sont →, ↓, ↑

$$\begin{pmatrix}
131 & 673 & \mathbf{234} & \mathbf{103} & \mathbf{18} \\
\mathbf{201} & \mathbf{96} & \mathbf{342} & 965 & 150 \\
630 & 803 & 746 & 422 & 111 \\
537 & 699 & 497 & 121 & 956 \\
805 & 732 & 524 & 37 & 331\\
\end{pmatrix}$$

Le chemin marqué en gras donne une somme de $994$ qui est ici le minimum possible pour cet exercice.

Écrire une fonction telle que `somme_minimale_2(grille)` renvoie la somme minimale spécifiée plus haut. On garantit que la `grille` est carrée et remplie d'entiers positifs.

!!! example "Exemples"

    $$\begin{pmatrix}
    5 & 2 & 3 \\
    \mathbf 2 & \mathbf 0 & 2 \\
    3 & \mathbf 2 & \mathbf 1 \\
    \end{pmatrix}$$
    
    ```pycon
    >>> grille = [[5, 2, 3], [2, 0, 5], [3, 2, 1],]
    >>> somme_minimale_2(grille)
    5
    ```

    $$\begin{pmatrix}
    9 & 9 & 0 & 9 & 9 \\
    \mathbf{3} & \mathbf{1} & 9 & 9 & 9 \\
    5 & \mathbf{1} & 9 & \mathbf{1} & \mathbf{3} \\
    7 & \mathbf{1} & 9 & \mathbf{1} & 9\\
    9 & \mathbf{1} & \mathbf{1} & \mathbf{1} & 9\\
    \end{pmatrix}$$

    ```pycon
    >>> grille = [
    ...     [9, 9, 0, 9, 9],
    ...     [3, 1, 9, 9, 9],
    ...     [5, 1, 9, 1, 3],
    ...     [7, 1, 9, 1, 9],
    ...     [9, 1, 1, 1, 9],
    ... ]
    >>> somme_minimale_2(grille)
    14
    ```

    $$\begin{pmatrix}
    131 & 673 & \mathbf{234} & \mathbf{103} & \mathbf{18} \\
    \mathbf{201} & \mathbf{96} & \mathbf{342} & 965 & 150 \\
    630 & 803 & 746 & 422 & 111 \\
    537 & 699 & 497 & 121 & 956 \\
    805 & 732 & 524 & 37 & 331\\
    \end{pmatrix}$$

    ```pycon
    >>> grille = [
    ...     [131, 673, 234, 103, 18],
    ...     [201, 96, 342, 965, 150],
    ...     [630, 803, 746, 422, 111],
    ...     [537, 699, 497, 121, 956],
    ...     [805, 732, 524, 37, 331],
    ... ]
    >>> somme_minimale_2(grille)
    994
    ```

{{ IDE('exo', MAX=1000) }}
