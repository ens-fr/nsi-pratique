def somme_minimale_2(grille):
    ...




# tests

grille = [[5, 2, 3], [2, 0, 5], [3, 2, 1]]
assert somme_minimale_2(grille) == 5


grille = [
    [9, 9, 0, 9, 9],
    [3, 1, 9, 9, 9],
    [5, 1, 9, 1, 3],
    [7, 1, 9, 1, 9],
    [9, 1, 1, 1, 9],
]
assert somme_minimale_2(grille) == 14


grille = [
    [131, 673, 234, 103, 18],
    [201, 96, 342, 965, 150],
    [630, 803, 746, 422, 111],
    [537, 699, 497, 121, 956],
    [805, 732, 524, 37, 331],
]
assert somme_minimale_2(grille) == 994
