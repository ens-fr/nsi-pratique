## Commentaires

{{ IDE('exo_corr') }}

Pour appliquer ici une stratégie « Diviser pour régner », on identifie une case de la grille rectangulaire qui permette, soit d'avoir trouvé, soit d'éliminer une colonne, soit d'éliminer une ligne. On peut choisir ou bien la case en haut à droite, ou alors la case en bas à gauche.
