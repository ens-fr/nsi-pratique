---
author: Franck Chambon
title: Grille triée
tags:
  - 4-grille
  - 5-diviser/régner
---
# Recherche dans une grille triée

On considère un tableau bidimensionnel d'éléments distincts, dont chaque ligne et chaque colonne est triée dans l'ordre croissant. On souhaite savoir si un élément `cible` est présent dans ce tableau.

!!! example "Exemple"
    Cette grille possède bien chaque ligne et chaque colonne triée.

    $$
    \begin{matrix}
        11 & 33 & \mathbf{42} & 63 \\
        20 & 52 & 67 & 80 \\
        25 & 61 & 88 & 95 \\
    \end{matrix}
    $$

    - $42$ est présente à la ligne 0, colonne 2.
    - $24$ est absent.


La grille ne sera pas donnée, mais vous avez à votre disposition trois fonctions telles que :

1. `nb_lignes(grille)` renvoie le nombre $n$ de lignes de la grille.
2. `nb_colonnes(grille)` renvoie le nombre $m$ de colonnes de la grille.
3. `donne_valeur(grille, i, j)` renvoie la valeur de `grille` à la ligne `i` et la colonne `j`.
4. `grille` **ne sera pas** une liste Python et vous n'avez aucun autre accès aux données.

On vous demande d'écrire une fonction telle que `recherche(cible, grille)` renvoie `None` si la `cible` est absente de `grille`, sinon le tuple `(i, j)` tel que `donne_valeur(grille, i, j)` est égal à la `cible`. On définit le cout de la recherche comme le nombre d'appels à la fonction `donne_valeur`.

On rappelle que la grille possède la propriété que chaque ligne et chaque colonne est triée.

Il existe plusieurs algorithmes pour une grille de $n$ lignes et $m$ colonnes, dont on donne le cout **dans le pire des cas** est :

1. Recherche exhaustive dans chaque case, dont le cout est $n×m$.
2. Recherche dichotomique sur chaque ligne dont le cout est environ $n\log(m)$.
3. Recherche dichotomique sur chaque colonne dont le cout est environ $m\log(n)$.
4. Recherche efficace, dont le cout est $m+n$.

On attend une recherche efficace. Une écriture itérative pourra être plus simple qu'une écriture récursive. Les deux sont possibles.

!!! example "Exemples"
    Avec la grille définie comme dans le premier test public, on a

    ```pycon
    >>> # script exécuté
    >>> recherche(42, grille)
    (0, 2)
    >>> recherche(24, grille) is None
    True
    ```


{{ IDE('exo', MAX=1000) }}

??? tip "Indice"
    En partant d'un des coins supérieur droit ou inférieur gauche :

    - soit on trouve la cible,
    - soit on peut éliminer une ligne entière ou une colonne entière

    Dans le pire des cas, en $n+m$ tentatives, on a éliminé toutes les lignes et toutes les colonnes.

    Après avoir éliminé une ligne ou une colonne, on se retrouve à chercher une cible dans une grille rectangulaire, ainsi on se retrouve dans une situation similaire à la précédente.
