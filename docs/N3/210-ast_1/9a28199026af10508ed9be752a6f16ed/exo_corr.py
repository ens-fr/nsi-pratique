SYMBOLES = "+-*/()"


def analyse(expression):
    elements = []
    temp = []
    for caractere in expression:
        if caractere in SYMBOLES:
            if len(temp) > 0:
                elements.append("".join(temp))
            elements.append(caractere)
            temp = []
        elif caractere != " ":
            temp.append(caractere)
    if len(temp) > 0:
        elements.append("".join(temp))
    return elements


# Tests
assert analyse("35.896 ") == ["35.896"]
assert analyse("3*   5+8") == ["3", "*", "5", "+", "8"]
assert analyse("3.9  * (5+8.6)") == ["3.9", "*", "(", "5", "+", "8.6", ")"]
