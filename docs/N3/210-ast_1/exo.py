SYMBOLES = "+-*/()"


def analyse(expression):
    ...


# Tests
assert analyse("35.896 ") == ["35.896"]
assert analyse("3*5   +8") == ["3", "*", "5", "+", "8"]
assert analyse("3.9  * (5+8.6)") == ["3.9", "*", "(", "5", "+", "8.6", ")"]
