def supprime_premier(chaine: str, cible: str) -> str:
    resultat = ""
    deja_vu = False
    for caractere in chaine:
        if caractere == cible and not deja_vu:
            deja_vu = True
        else:
            resultat += caractere
    return deja_vu, resultat


def anagrammes(chaine1: str, chaine2: str) -> bool:
    if len(chaine1) == 1:
        return chaine1 == chaine2
    else:
        cible = chaine1[0]
        vrai, chaine1_sans_cible = supprime_premier(chaine1, cible)
        cible_dans_2, chaine2_sans_cible = supprime_premier(chaine2, cible)
        if cible_dans_2:
            return anagrammes(chaine1_sans_cible, chaine2_sans_cible)
        else:
            return False


# Tests
assert supprime_premier('ukulélé', 'u') == (True, 'kulélé')
assert supprime_premier('ukulélé', 'é') == (True, 'ukullé')
assert supprime_premier('ukulélé', 'a') == (False, 'ukulélé')
assert anagrammes('chien', 'niche')
assert anagrammes('énergie noire', 'reine ignorée')
assert not anagrammes('louve', 'poule')
