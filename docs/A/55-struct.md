# Structures de données

On y utilise ou met en œuvre des listes chainées, piles, files, arbres...

* [Train en POO](../N2/700-poo_train/sujet.md) : une liste chainée en réalité
* [Bien parenthésée 2](../N3/730-expression_bien_parenthesee_2/sujet.md) : une expression est-elle bien parenthésée ?
* [Hauteur et taille d'un arbre](../N1/860-arbre_enracine/sujet.md) : on y représente les arbres sous forme de liste de listes Python
* [Hauteur et taille d'un arbre binaire](../N2/800-arbre_bin/sujet.md) : on y représente les arbres à l'aide de la programmation orientée objet
