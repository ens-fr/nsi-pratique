# Tris

> Retrouver le [Cours sur les tris](#){ target="_blank" rel="noopener" .md-button } (bientôt)

Les classiques !

- [Tri par sélection](../N1/500-tri_selection/sujet.md)
- [Tri à bulles](../N2/500-tri_bulles/sujet.md)
- [Insertion dans une liste triée](../N2/180-insertion_liste_triee/sujet.md)
- [Fusion de listes triées](../N2/200-fusion_listes_triees/sujet.md)
- [Tas min](../N3/810-tas_min/sujet.md) : vérifier qu'un tableau représente un tas minimal
