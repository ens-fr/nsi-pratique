# Programmation orientée objet

Il s'agit ici d'utiliser des classes proposées ou de les écrire.

* [Chien en POO](../N2/700-poo_chien/sujet.md) : compléter une classe représentant un chien
* [Train en POO](../N2/700-poo_train/sujet.md) : compléter une classe représentant un train
* [Hauteur et taille d'un arbre binaire](../N2/800-arbre_bin/sujet.md) : compléter les méthodes `hauteur` et `taille`
* [Arbre binaire de recherche](../N2/876-ABR/sujet.md) : compléter les méthodes `insere`, `est_present`, `affichage_infixe`
