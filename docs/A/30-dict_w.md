# Construction de dictionnaires

- [Valeurs extrêmes](../N1/300-dict_extremes/sujet.md) : extraire le dictionnaire `{"mini": m, "maxi": M}` d'un tableau
- [Dictionnaire d'occurrences](../N1/320-dico_occurrences/sujet.md) : compter les occurrences des caractères dans une chaine
- [Dictionnaire des antécédents](../N1/369-antecedents/sujet.md) : déterminer le dictionnaire « réciproque » d'un dictionnaire : `{valeur: [clés associées]}`
