# Récursivité

> Retrouver le [Cours sur la récursivité](https://e-nsi.forge.aeif.fr/recursif/){ target="_blank" rel="noopener" .md-button }

## Peu de mathématiques

- [Suite de Fibonacci (1)](../N1/600-fib_1/sujet.md) : une fonction très élémentaire, pour des indices inférieurs à 25
- [Anagrammes](../N3/350-anagrammes/sujet.md) : deux chaines sont-elles des anagrammes ?
- [Percolation](../N2/666-percolation/sujet.md) : écoulement d'eau dans un sol, ou plutôt parcours en profondeur dans une grille
- [Pavage possible avec triominos (2)](../N3/654-pavage_triomino_2/sujet.md) : Construire un pavage, si possible, d'un carré troué avec des triominos

## Exercices guidés, plus de mathématiques

- [Énumération des permutations](../N2/741-nb_factoriel/sujet.md) : les nombres factoriels
- [Énumération des chemins à Manhattan](../N2/764-nb_chemins_grille/sujet.md) : relier deux points en ne se déplaçant que vers la droite ou vers le haut
- [Énumération des chemins de Schröder](../N2/764-nb_schroder/sujet.md) : mouvements ↗ , →→, ou ↘
- [Énumération des chemins de Delannoy](../N2/765-nb_delannoy/) : mouvements ↗, →, ou ↘
- [Énumération des arbres binaires de taille n](../N2/850-nb_catalan_2/sujet.md) : les nombres de Catalan
- [Énumération des arbres unaires-binaires à n+1 nœuds](../N2/870-nb_motzkin/sujet.md) : les nombres de Motzkin
- [Énumération des subdivisions de polygone](../N4/764-nb_hipparque/sujet.md) : Les nombres d'Hipparque
- [Suite de Fibonacci (2)](../N4/640-fib_2/sujet.md) :boom: :boom: : Calcul possible du millionième terme

## Exercices non guidés, mais avec indices

- [Nombre de partitions d'un entier](../N4/640-nb_partitions/sujet.md) :boom: :boom: :boom:
