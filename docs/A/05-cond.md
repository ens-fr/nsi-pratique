# Structures conditionnelles

Si c'est vrai il faut faire ceci, sinon cela...

- [Multiplier sans *](../N1/140-multiplication/sujet.md) : multiplier deux entiers relatifs sans utiliser le symbole `*`
- [Suite de Syracuse](../N1/400-syracuse/sujet.md) : la fameuse suite $3n+1$
- [Tous différents](../N1/420-tous_differents/sujet.md) : les éléments d'un tableau sont-ils tous distincts (solution en temps quadratique)
