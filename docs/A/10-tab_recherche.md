# Recherche dans un tableau

On recherche ici les _extrema_ dans un tableau (minimum ou maximum), une valeur ou un indice particulier.

- [Maximum](../N1/110-maximum_nombres/sujet.md) : déterminer le maximum dans un tableau
- [Indice du minimum](../N1/100-ind_min/sujet.md) : déterminer l'indice du minimum dans un tableau
- [Premier minimum local](../N1/122-premier_minimum/sujet.md) : déterminer l'indice du premier minimum dans un tableau
- [Occurrences du minimum](../N1/130-occurrences_du_mini/sujet.md) : déterminer la valeur et les indices du minimum
- [Valeur et indices du max](../N1/200-val_ind_max/sujet.md) : déterminer la valeur et les indices du maximum
