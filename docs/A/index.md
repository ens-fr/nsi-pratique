# 🧭 Suggestions

Ce site est organisé en différentes catégories permettant à chacun de progresser à son rythme.

Exercices :

- très simples,
- simples, mais non guidés,
- guidés, un peu plus élaborés,
- non guidés et plus consistants,
- colorés mathématiquement

Il peut aussi être intéressant de traiter des exercices d'un thème particulier :

- fondamentaux du langage,
- structures de données plus ou moins élaborées,
- algorithmes classiques,
- paradigmes différents.

Cette section vise à présenter les exercices sous cet angle.

